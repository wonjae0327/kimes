package kr.co.sysforu.kimes.mappers.notice;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Mapper
public interface NoticeMapper {

    List<Map<String, Objects>> selectNoticeKr();

    List<Map<String, Objects>> selectNoticeKr1();

    List<Map<String, Objects>> selectNoticeKr2();

    List<Map<String, Objects>> selectNoticeKrNews();
}
