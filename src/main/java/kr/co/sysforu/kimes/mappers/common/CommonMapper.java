package kr.co.sysforu.kimes.mappers.common;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CommonMapper {
    List<Map<String, Object>> getCode(String year);

    List<Map<String, Object>> getTbCode(String year);
}
