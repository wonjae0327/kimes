package kr.co.sysforu.kimes.service;

import java.util.List;
import java.util.Map;

public interface CommonService {

    Map<String, Object> getCode();

    List<Map<String, Object>> getTbCode();
}
