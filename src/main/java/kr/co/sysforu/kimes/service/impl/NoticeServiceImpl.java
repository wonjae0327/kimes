package kr.co.sysforu.kimes.service.impl;

import kr.co.sysforu.kimes.mappers.notice.NoticeMapper;
import kr.co.sysforu.kimes.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    NoticeMapper noticeMapper;

    @Override
    public List<Map<String, Objects>> selectNoticeKr() {
        return noticeMapper.selectNoticeKr();
    }

    @Override
    public List<Map<String, Objects>> selectNoticeKr1() {
        return noticeMapper.selectNoticeKr1();
    }

    @Override
    public List<Map<String, Objects>> selectNoticeKr2() {
        return noticeMapper.selectNoticeKr2();
    }

    @Override
    public List<Map<String, Objects>> selectNoticeKrNews() {
        return noticeMapper.selectNoticeKrNews();
    }
}
