package kr.co.sysforu.kimes.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public interface NoticeService {

    List<Map<String, Objects>> selectNoticeKr();

    List<Map<String, Objects>> selectNoticeKr1();

    List<Map<String, Objects>> selectNoticeKr2();

    List<Map<String, Objects>> selectNoticeKrNews();
}
