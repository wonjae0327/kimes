package kr.co.sysforu.kimes.service.impl;

import kr.co.sysforu.kimes.mappers.common.CommonMapper;
import kr.co.sysforu.kimes.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CommonServiceImpl implements CommonService {
    @Autowired
    CommonMapper commonMapper;
    private String year = String.valueOf(LocalDate.now().getYear());

    @Override
    public Map<String, Object> getCode() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> codeList = commonMapper.getCode(year);

        for (Map<String, Object> map : codeList) {
            resultMap.put(map.get("CD_CODE").toString(), map.get("CD_VALUE_KR"));
        }

        return resultMap;
    }

    @Override
    public List<Map<String, Object>> getTbCode() {
        return commonMapper.getTbCode(year);
    }
}
