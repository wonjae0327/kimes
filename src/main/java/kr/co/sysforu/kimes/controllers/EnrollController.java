package kr.co.sysforu.kimes.controllers;

import kr.co.sysforu.kimes.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/enroll")
public class EnrollController {
    @Autowired
    CommonService commonService;

    @GetMapping("/regist")
    public String regist(Model model) {
        model.addAttribute("code", commonService.getCode());

        return "enroll/regist";
    }

    @GetMapping("/regist_form")
    public String regist_form(Model model) {
        model.addAttribute("code", commonService.getCode());
        model.addAttribute("codes", commonService.getTbCode());
        return "enroll/regist_form";
    }
}
