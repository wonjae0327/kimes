package kr.co.sysforu.kimes.controllers;

import kr.co.sysforu.kimes.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @Autowired
    NoticeService noticeService;

    @GetMapping("/")
    String index(Model model) {
        model.addAttribute("notices2", noticeService.selectNoticeKr2());
        model.addAttribute("notices", noticeService.selectNoticeKr());
        model.addAttribute("notices1", noticeService.selectNoticeKr1());
        model.addAttribute("noticeNews", noticeService.selectNoticeKrNews());

        return "index";
    }

}
