/* Image _on_off */
function on(){this.src = this.src.replace("_off","_on");}
function off(){this.src = this.src.replace("_on","_off");}

/* Banner Slide */
function bannerSlide( overallBox, ctrBox, rollSpeed, autoMove, autoRollTerm, rollType){

	var that = {
		$overallBox : $(overallBox),
		$slideBox : $(overallBox + " ul"),
		$slideBoxItem : $(overallBox + " ul li"),
		$obj : null,
		itemNum : $(overallBox + " li").length,
		$ctrBox : $(ctrBox),
		_width : $(overallBox).width(),
		_height : $(overallBox + " ul").height(),
		presentNum : 0,
		autoState : null,
		rollSpeed : rollSpeed,
		autoRollTerm : autoRollTerm,
		rollType : rollType,

		moveSlide : function( targetIdx ){
			if ( targetIdx == this.presentNum )		return false;

			var direction;

			if( targetIdx > this.presentNum)	direction = "right";
			else								direction = "left";

			targetIdx = ( targetIdx + this.itemNum ) % this.itemNum;

			if( direction == 'right'){
				if( this.rollType == "fading"){
					this.$obj[this.presentNum].css({'z-index' : 2}).stop().animate({'opacity':1}, this.rollSpeed, function(){ $(this).css("z-index", 1)});
					this.$obj[targetIdx].css({'z-index' : 3, 'opacity' : 0}).stop().animate({'opacity':1}, this.rollSpeed);
				}else if( this.rollType == "vertical"){
					this.$obj[this.presentNum].css('top', 0 ).stop().animate({'top':(-this._height)}, this.rollSpeed);
					this.$obj[targetIdx].css('top', this._height ).stop().animate({'top':0}, this.rollSpeed);
				}
				else{
					this.$obj[this.presentNum].css('left', 0 ).stop().animate({'left':(-this._width)}, this.rollSpeed);
					this.$obj[targetIdx].css('left', this._width ).stop().animate({'left':0}, this.rollSpeed);
				}
			}else{
				if( this.rollType == "fading"){
					this.$obj[this.presentNum].css({'z-index' : 2}).stop().animate({'opacity':1}, this.rollSpeed, function(){ $(this).css("z-index", 1)});
					this.$obj[targetIdx].css({'z-index' : 3, 'opacity' : 0}).stop().animate({'opacity':1}, this.rollSpeed);
				}else if( this.rollType == "vertical"){
					this.$obj[this.presentNum].css('top', 0 ).stop().animate({'top':this._height}, this.rollSpeed);
					this.$obj[targetIdx].css('top', -this._height ).stop().animate({'top':0}, this.rollSpeed);
				}else{
					this.$obj[this.presentNum].css('left', 0 ).stop().animate({'left':this._width}, this.rollSpeed);
					this.$obj[targetIdx].css('left', -this._width ).stop().animate({'left':0}, this.rollSpeed);
				}
			}

			this.presentNum = targetIdx;

			this.$ctrBox.find("a").removeClass("on").find("img").each(off);
			this.$ctrBox.find("a").eq(targetIdx).addClass("on").find("img").each(on);
		}
	}

	that.init = function(){

		this.$overallBox.css("positon", "relative");

		this.$obj = new Array();

		for(i=0; i<this.itemNum; i++){
			this.$obj[i] = this.$slideBoxItem.eq(i);

			if( this.rollType == "fading" ){
				this.$obj[i].css({
					"float" : "none",
					"position" : "absolute",
					"left" : 0,
					"top" : 0,
					"z-index" : 1
				});
			}
			else if( this.rollType == "vertical" ){
				this.$obj[i].css({
					"float" : "none",
					"position" : "absolute",
					"left" : 0,
					"top" : this._height,
					"z-index" : 1
				});
			}else{
				this.$obj[i].css({
					"float" : "none",
					"position" : "absolute",
					"left" : this._width,
					"top" : 0,
					"z-index" : 1
				});
			}
		}

		this.$obj[ this.presentNum ].css({"left" : 0, "top" : 0, "z-index" : 3, "display" : "block"});

		if( ctrBox != null ){
			this.$ctrBox.find("a").removeClass("on").find("img").each(off);
			this.$ctrBox.find("a").eq(0).addClass("on").find("img").each(on);

			this.$ctrBox.find("a").click(function(e){
				e.preventDefault();
				that.moveSlide( $(this).index() );
			});
		}

		if ( autoMove != null ){
			this.autoState = setInterval(function(){ that.moveSlide( that.presentNum + 1 ); }, that.autoRollTerm);

			this.$overallBox.bind("mouseenter", function(){
				clearInterval( that.autoState );
			});

			this.$overallBox.bind("mouseleave", function(){
				that.autoState = setInterval(function(){ that.moveSlide( that.presentNum + 1 ); }, that.autoRollTerm);
			});
		}

		if( this.$overallBox.find(".prev").length > 0 ){
			this.$overallBox.find(".prev").click(function(e){
				e.preventDefault();
				that.moveSlide( that.presentNum - 1 );
			});
		}

		if( this.$overallBox.find(".next").length > 0 ){
			this.$overallBox.find(".next").click(function(e){
				e.preventDefault();
				that.moveSlide( that.presentNum + 1 );
			});
		}
	}

	that.init();

	return that;
}

/* Banner Slide2 */
function bannerSlide2(overallBox, ctrBox, rollSpeed){

	var that = {
		$overallBox : $(overallBox),
		$slideBox : $(overallBox + " ul"),
		$slideBoxItem : $(overallBox + " ul li"),
		$obj : null,
		itemNum : $(overallBox + " li").length,
		$ctrBox : $(ctrBox),
		TotalWidth : 0,
		minLeft : 0,
		maxLeft : 0,
		presentLeft : 0,
		rollSpeed : rollSpeed,
		direction : "right",
		step : 2,
		itv : null,

		moveSlide : function(){

			if( this.direction == "left"){
				if(	this.presentLeft >= this.minLeft )		this.presentLeft = this.maxLeft;
				else										this.presentLeft += this.step;
			}else{
				if(	this.presentLeft <= this.maxLeft )		this.presentLeft = this.minLeft;
				else										this.presentLeft -= this.step;
			}

			this.$slideBox.css("margin-left", this.presentLeft );
		}
	}

	that.init = function(){

		for(i=0;i<this.itemNum;i++)
			this.TotalWidth += this.$slideBoxItem.eq(i).outerWidth();

		this.presentLeft = -this.TotalWidth;
		this.minLeft = -this.TotalWidth;
		this.maxLeft = -this.TotalWidth * 2;

		this.$slideBox.prepend ( this.$slideBoxItem.clone() );
		this.$slideBox.append ( this.$slideBoxItem.clone() );

		this.itv = setInterval( function(){ that.moveSlide(); }, rollSpeed );

		this.$slideBox.bind("mouseenter", function(e){ clearInterval( that.itv ); });

		this.$slideBox.bind("mouseleave", function(e){ that.itv = setInterval( function(){ that.moveSlide(); }, rollSpeed ); });

		this.$ctrBox.find(".prev").click(function(e){
			e.preventDefault();
			that.direction = "left"
		});

		this.$ctrBox.find(".next").click(function(e){
			e.preventDefault();
			that.direction = "right"
		});
	}

	that.init();

	return that;
}

/* LNB */
function lnb(depth2, depth3){
	var $lnbObj = $(".lnb"),
		$lnb2Li = $(".lnb > ul > li"),
		$lnb3Ul = $(".lnb > ul > li > ul"),
		$lnb3Li = $(".lnb > ul > li > ul > li"),
		timeUnit;

	function resetLnb(){
		$lnb2Li.removeClass("on").find("> a > img").each(off);
		$lnb3Li.removeClass("on").find("> a > img").each(off);
		$lnb3Ul.stop().slideUp();

		$lnb2Li.eq(depth2).addClass("on").find("> a > img").each(on);

		if (depth3 > -1 ){
			$lnb2Li.eq(depth2).find("> ul").stop().slideDown();
			$lnb2Li.eq(depth2).find("> ul > li").eq(depth3).addClass("on").find("> a > img").each(on);
		}
	}

	resetLnb();

	$lnb2Li.bind("mouseenter focusin", function(){
		clearTimeout(timeUnit);

		$(this).siblings().removeClass("on").find("> a > img").each(off);
		$(this).addClass("on").find("> a > img").each(on);

		var $dep3Ul = $(this).find("> ul");

		$lnb3Ul.stop().slideUp();

		if ( $dep3Ul.length > 0 ){
			$dep3Ul.stop().slideDown();
		}
	});

	$lnb3Li.bind("mouseenter focusin", function(){
		clearTimeout(timeUnit);
		$lnb3Li.removeClass("on").find("> a > img").each(off);
		$(this).addClass("on").find("> a > img").each(on);
	});

	$(".lnb").bind("mouseleave focusout", function(){
		timeUnit = setTimeout( function(){resetLnb();}, 500);
	});
}

/* GNB */
////function gnb(depth1, depth2, depth3){
//
//	var $gnbWrap = $(".gnb_wrap"),
//		$gnbBG = $(".gnb_on"),
//		$gnbObj = $(".gnb"),
//		$gnbDepth1 = $(".gnb > li"),
//		$gnbDepth2Ul = $(".gnb li ul"),
//		$gnbDepth2 = $(".gnb li ul li"),
//		max_height = 335,
//		aniTime = 400,
//		autoTime;
//
//	/* GNB Activate, Deactivate*/
//	$gnbObj.bind("mouseenter focusin", gnb_on);
//	$gnbWrap.bind("mouseleave focusout", autoMenu);
//
//	function gnb_on(){
//		$gnbBG.css("display", "block").stop().animate({"height" : max_height}, aniTime);
//		$gnbDepth2Ul.css("display", "block").stop().animate({"height" : max_height}, aniTime);
//	}
//
//	// function gnb_off(){
//	// 	$gnbBG.stop().animate( {"height" : 0}, aniTime, function(){ $(this).hide(); } );
//	// 	$gnbDepth2Ul.stop().animate( {"height" : 0}, aniTime, function(){ $(this).hide(); } );
//	// }
//
//	/* GNB Depth1 - On, Off*/
//	$gnbDepth1.bind("mouseenter focusin", function(){ $(this).addClass("on").find(".gnb > li > a").each(on);	});
//	$gnbDepth1.bind("mouseleave focusout", function(){ $(this).removeClass("on").find(".gnb > li > a").each(off); });
//
//	/* GNB Depth2 - On, Off*/
//	$gnbDepth2.bind("mouseenter focusin", function(){
//		$(this).addClass("on").find(".gnb > li > a").each(on);
//		$(this).stop().animate({ "marginLeft": "7px" }, 800, "easeOutExpo");
//	});
//	$gnbDepth2.bind("mouseleave focusout", function(){
//		$(this).removeClass("on").find(".gnb > li > a").each(off);
//		$(this).stop().animate({ "marginLeft": "0" }, 800, "easeOutExpo");
//	});
//
//	if ( depth2 > -1 && depth3 > -1)		lnb( depth2, depth3 );
//	else if( depth2 > -1 )					lnb( depth2, -1 );
//
//	/* 자동 활성화 메뉴(서브 전용) */
//	function autoMenu(){
//		var idx = depth1;
//		var sidx = depth2;
//		if (idx > -1 ){
//			$(".gnb > li:eq("+idx+")").addClass('on')
//			$(".gnb > li:eq("+idx+") > ul > li:eq("+sidx+")").addClass('on')
//			$gnbBG.stop().animate( {"height" : 0}, aniTime, function(){ $(this).hide(); } );
//			$gnbDepth2Ul.stop().animate( {"height" : 0}, aniTime, function(){ $(this).hide(); } );
//		}
//		else{
//			$gnbBG.stop().animate( {"height" : 0}, aniTime, function(){ $(this).hide(); } );
//			$gnbDepth2Ul.stop().animate( {"height" : 0}, aniTime, function(){ $(this).hide(); } );
//		}
//	}
//	autoTime = setTimeout( function(){autoMenu();}, 500);
//}

/* flash */
function makeFlashScript(width, height, classid, filename)
{
	document.write("<object classid='clsid:"+classid+" codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0' wmode='transparent' width='"+width+"' height='"+height+"'>");
	document.write("  <param name='movie' value='"+filename+"'>");
	document.write("  <param name='quality' value='high'>");
	document.write("  <param name='wmode' value='transparent'>");
	document.write("  <embed src='"+filename+"' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' wmode='transparent' width='"+width+"' height='"+height+"'></embed>");
	document.write("</object>");
}

/* 메인 비주얼 모션 */
function mainVisualMotion(){

	var mObj = new Array(".building01", ".building02", ".building63", ".tower", ".cheom", ".gate01", ".gate02", ".building00");
	var defaultPosArr = new Array(".building01", ".building02", ".building63", ".tower", ".cheom", ".gate01", ".gate02", ".building00");
	var movePosArr = new Array(".building01", ".building02", ".building63", ".tower", ".cheom", ".gate01", ".gate02", ".building00");

	//초기화
	function inti(){
		$(".vis_txt").css({"top" : "40px"});
		motionStart();
		movePos();
	}
	function motionStart() {
		//txtShadow
		var ranDomArray = new Array();
		var len;

		$(".vis_cont .vis_obj").each(function(){
			var idx = $(this).index()
			ranDomArray[idx] = idx-1;
			len = ranDomArray.length-1;
		});

		$(".vis_cont .vis_obj").each(function(){
			var idx = $(this).index()
			var obj = new Array();
			var ran = Math.floor(Math.random()*Number(len-idx)+idx);
			var temp = ranDomArray[ran];

			ranDomArray[ran] = ranDomArray[idx];
			ranDomArray[idx] = temp;
			//TweenMax.killTweensOf($(".vis_cont .vis_obj"));
			TweenMax.to($(".vis_cont .vis_obj:eq("+ranDomArray[idx]+")"),2.5, {css:{opacity:1},delay:idx *.1,ease:Expo.easeOut});
		});
		defaultPos()
		textMotion()
		cloud()

		return ranDomArray;
	};

	function defaultPos() {
		//오브젝트 초기 bottom 위치 저장
		for (var i = 0; i < defaultPosArr.length; i++) {
			defaultPosArr[i] = $(defaultPosArr[i]).css("bottom");
			TweenMax.to($(mObj[i]),2.5, {css:{bottom:0},ease:Expo.easeOut});
		}
	}

	function movePos(){
		//오브젝트 이동 위치
		for (var j = 0; j < defaultPosArr.length; j++) {
			movePosArr[j] = $(movePosArr[j]).css("bottom","-40px");
		}
	}

	function textMotion(){
		TweenMax.to($(".vis_txt"),2.5, {css:{top:70,opacity:1},delay:.5,ease:Expo.easeOut});
	}

	function cloud(){
		TweenMax.to($(".cloud01"), 1, {left:"250px", repeat:-1, yoyo:true,delay:.5});
		TweenMax.to($(".cloud02"), 1, {left:"300px", repeat:-1, yoyo:true});
		TweenMax.to($(".cloud03"), 1, {right:"105px", repeat:-1, yoyo:true,delay:.2});
		TweenMax.to($(".cloud04"), 1, {right:"15px", repeat:-1, yoyo:true});
	}

	inti()
}


/* 메인 롤링배너 */
function rollBanner(){
	var bxSlider = $('#rollBanner').bxSlider({
        auto: true,
        speed: 500,
        onSlideNext: function() {
            $(".control a").removeClass("on")
            $(".control a:eq("+bxSlider.getCurrentSlide()+")").addClass("on")

        },
        onSlidePrev: function() {
            $(".control a").removeClass("on")
            $(".control a:eq("+bxSlider.getCurrentSlide()+")").addClass("on")
        }

    });

    //현재 버튼
    $(".control a").on("click",function(e){
		e.preventDefault();
        var idx = $(this).index();
        bxSlider.goToSlide(idx);
        $(".control a").removeClass("on")
        $(".control a:eq("+bxSlider.getCurrentSlide()+")").addClass("on")
     })
}

// accodianUI(아코디언)
function accordianList(){
	var el;

	el = $('.accd');

	if(el.length <= 0){
		return;
	}

	el.each(function(idx, obj){
		$(obj).find('.cont_in').removeClass('on');

		bindEvents();
		function bindEvents(){
			$(obj).find('>a').off('click.accodianEvt').on('click.accodianEvt', function(e){
				e.preventDefault();

				var index = $(this).closest('.accd').index();

				$(obj).each(function(){
						if($(obj).hasClass('on')){
							$(obj).removeClass('on');
							$(obj).find('> a').removeClass('on');
							$(obj).find('> .cont_in').removeClass('on');
						}else{
							$(obj).addClass('on');
							$(obj).find('> a').addClass('on');
							$(obj).find('> .cont_in').addClass('on');
						}
				});
			});
		}
	});
}


// accodianUI(아코디언)
function accodianUI() {
	var el;

	el = $('.accdMulti');

	if(el.length <= 0){
		return;
	}

	el.each(function(idx, obj){
		//$(obj).find('>ul>li').removeClass('on');

		bindEvents();
		function bindEvents(){
			$(obj).find('>ul>li>a.accd_btn').off('click.accodianEvt').on('click.accodianEvt', function(e){
				e.preventDefault();

				var index = $(this).closest('li').index();

				$(obj).find('>ul>li').each(function(idx, obj){
					if(idx == index){
						if($(obj).hasClass('on')){
							$(obj).removeClass('on');
						}else{
							$(obj).addClass('on');
						}
					}else{
						$(obj).removeClass('on');
					}
				});

			});
		}
	});
}

//gnb_wrap 고정
/*
function gnb_wrap(){
	var gnbWrap = $('#header');

	if(gnbWrap.length <= 0){
		return;
	}
	//전체 스크롤시
	$(window).off('scroll.gnbScroll').on('scroll.gnbScroll', function(){
		if($(this).scrollTop() >= gnbWrap.find('.header_cont').outerHeight(true) + 34){
			gnbWrap.addClass('fixed');
		}else{
			gnbWrap.removeClass('fixed');
		}
	});
}
*/

/* 메인 롤링배너 */
function mainVisBanner(){
	var bxSlider = $('.main_vis .slide').bxSlider({
        auto: true,
        speed: 500,
        onSlideNext: function() {
            $(".control a").removeClass("on")
            $(".control a:eq("+bxSlider.getCurrentSlide()+")").addClass("on")

        },
        onSlidePrev: function() {
            $(".control a").removeClass("on")
            $(".control a:eq("+bxSlider.getCurrentSlide()+")").addClass("on")
        },
    });

    $('.main_vis .control_go a.gonext').click(function(){
      bxSlider.goToNextSlide();
      return false;
    });

    $('.main_vis .control_go a.goprev').click(function(){
      bxSlider.goToPrevSlide();
      return false;
    });

    //현재 버튼
    $(".control a").on("click",function(e){
		e.preventDefault();
        var idx = $(this).index();
        bxSlider.goToSlide(idx);
        $(".control a").removeClass("on")
        $(".control a:eq("+bxSlider.getCurrentSlide()+")").addClass("on")
     })
	var mql = window.matchMedia("screen and (max-width: 768px)"); 
	mql.addListener(function(e) { if(!e.matches) { bxSlider.reloadSlider(); } });
}


/* 후원 슬라이드 */
function sponsorSlider(){
	var bxSlider = $('.sponsor_wrap .slide').bxSlider({
		auto: true,
		speed: 500,
		slideWidth : '188',
		infiniteLoop: false,
	});

	$('.sponsor_wrap .control_go .gonext').click(function(e){
		e.preventDefault()
		bxSlider.goToNextSlide();
		return false;
	});

	$('.sponsor_wrap .control_go .goprev').click(function(e){
		e.preventDefault()
		bxSlider.goToPrevSlide();
		return false;
	});

	var mql = window.matchMedia("screen and (max-width: 768px)"); 
	mql.addListener(function(e) { if(!e.matches) { bxSlider.reloadSlider(); } });
}


// scrollTab(가로 스크롤 탭)
function scrollSlide(){
	var el;

	el = $('.news_wrap');

	el.each(function(idx , obj){
		var $ssWrap = $(obj);
		var $ssUL = $ssWrap.find('> .slide');
		var $ssItem = $ssUL.find('> li');

		$ssItem.eq(0).addClass('active');

		if( $(window).width() <= 425){
			$ssUL.each(function(){
				var totalWidth = 0;
				$ssItem.each(function(){
					var itemW = $(this).outerWidth();
					totalWidth += itemW;
					$ssUL.outerWidth(totalWidth);	// slide width 부여
				});
			});
		}


		$(window).on('load' , function(){
			var goprev =  $(obj).find('.control_go .goprev')
			var gonext =  $(obj).find('.control_go .gonext')

			gonext.click(function(e){
				e.preventDefault();
				var currentActive = $(obj).find('.slide > li.active');
				currentActive.removeClass('active');

				if (currentActive.is(':last-child')) {
					$(obj).find('.slide > li').last().addClass('active');
				} else {
					currentActive.next('li').addClass('active');
				}
				bindEvents();
			});

			goprev.click(function(e){
				e.preventDefault();
				var currentActive = $(obj).find('.slide > li.active');
				currentActive.removeClass('active');

				if (currentActive.is(':first-child')) {
					$(obj).find('.slide > li').first().addClass('active');
				} else {
					currentActive.prev('li').addClass('active');
				}
				bindEvents();
			});

			function bindEvents(){
				// li.on scroll Left
				var wrapPosLeft = $ssWrap.position().left;
				var posLeft = $ssWrap.find('.slide > li.active').position().left
				$ssWrap.scrollLeft(
					posLeft - 15
				);
			}

		});
	});
}

// 전체 메뉴 
function allmenu(){
	var el;

	el = $('#header');

	el.each(function(idx , obj){
		el.find('.menu').on('click' , function(e){
			e.preventDefault();
			if( $(obj).hasClass('gnb_open') ){
				$(obj).removeClass('gnb_open');
				$('body').removeClass('open_pop');
				$('.header_cont').find('.dim').remove();
			}else{
				$(obj).addClass('gnb_open');
				$('body').addClass('open_pop');
				$('.header_cont').append('<span class="dim"></span>');
			}
		});

		// 전체메뉴 닫기
		$(document).on('click' , '.close_gnb , .dim' , function(e){
			e.preventDefault();
			$(obj).removeClass('gnb_open');
			$('body').removeClass('open_pop');
			$('.header_cont').find('.dim').remove();
		});
	});
}

//img click viewer box
function imgViewList(){
	$('.gallery_list').each(function(){
		var firstSrc = $(this).find('.gallery_thum > ul > li.on img').attr('src');
		$(this).find('.gallery_view > img').attr('src' , firstSrc)

		$('.gallery_thum > ul > li > a').on('click' , function(e){
			e.preventDefault();
			var src = $(this).find('>img').attr('src');
			$(this).closest('.gallery_list').find('.gallery_thum ul li').removeClass('on');
			$(this).parent().addClass('on');
			$(this).closest('.gallery_list').find('.gallery_view > img').attr('src', src);
			
		});
	})
}


//gnb			===========================================================
function gnb(dep,type){

	var $header = $('#header')
	var $gnbWrapBg = $('.gnb_wrap')
	var $gnb = $('.gnb')
	var $gnbLi = $gnb.find('> ul > li')
	var $gnb2Dep = $gnbLi.find('.dep01')
	var $gnb2DepLi = $gnb2Dep.find('li')
	var $gnb2DepLiA = $gnb2DepLi.find('> a')
	var $lnbLi = $('.lnb').find('> li')
	var dep1 = dep[0]
	var dep2 = dep[1]
	var dep3 = dep[2]

	removeActive();


	if( $(window).width() >= 769 ){

	$header.on('mouseleave' , function(){
		$header.removeClass("gnb_open");
		$header.removeAttr('style');
		removeActive()
	})

	$gnbLi.find(">a").on({
		"mouseenter" : function(e){
			e.preventDefault();
			$header.addClass("gnb_open");
			$gnbWrapBg.removeClass("path");
			$gnbWrapBg.addClass("hover");
			$gnb2Dep.removeClass('on')
			$gnbLi.removeClass('on');
			$(this).parent().addClass('on').find($gnb2Dep).addClass('on');
			$gnb2Dep.addClass('on');
		}
	});
	}else{

		$gnb.on('mouseenter' , function(e){
			e.preventDefault();
			$header.addClass("gnb_open");
		})

		$gnbLi.find(">a").on({
			"click" : function(e){
				e.preventDefault();
				$header.addClass("gnb_open");
				$gnbWrapBg.removeClass("path");
				$gnbWrapBg.addClass("hover");
				$gnb2Dep.removeClass('on')
				$gnbLi.removeClass('on');
				$(this).parent().addClass('on').find($gnb2Dep).addClass('on');
				$gnb2Dep.addClass('on');
			}
		});
	}
	
	

	//common removeActive
	function removeActive(){
		if(!$gnbWrapBg.hasClass("path")) {
			$header.removeClass("gnb_open");
			$gnb2Dep.removeClass('on')
			$gnbWrapBg.removeClass("hover");
			$gnbWrapBg.addClass("path");
		}
		$gnbLi.removeClass('on');
		$gnbLi.eq(dep1 - 1).addClass('on');
		$gnbLi.eq(dep1 - 1).find($gnb2DepLi).eq(dep2 - 1).addClass('on');
		$lnbLi.eq(dep3 - 1).addClass('on');
	}
}

$(function(){
	accordianList();
	accodianUI();
	//gnb_wrap();
	mainVisBanner();
	sponsorSlider();
	scrollSlide();
	allmenu();
	imgViewList();
});

