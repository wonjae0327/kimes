$(document).ready(function(){ 
	
	/* 공지사항 + 지원사업 게시물 노출비노출 */
	$('#notice_bbs').css('display','block');
	$('#project_bbs').css('display','none');

	$('.notice_bbs_btn').click(function(){ 
		$('#notice_bbs').css('display','none');
		$('#project_bbs').css('display','block');

	});
	$('.project_bbs_btn').click(function(){ 
		$('#notice_bbs').css('display','block');
		$('#project_bbs').css('display','none');

	});


	/* 라이브 유튜브 날짜별 영상 교체 */
	$('.date_ten').addClass('act');

	$('.date_ten').click(function(){ 
		$('#youtube_live').attr('src','https://www.youtube.com/embed/gCPR6kJUYKw');
		$('.date_ten').addClass('act');
		$(".date_eleven").removeClass('act');
		$(".date_twelve").removeClass('act');
	});
	$('.date_eleven').click(function(){ 
		
		$('#youtube_live').attr('src','https://www.youtube.com/embed/nHgj66liF8Y');
		$(".date_eleven").addClass('act');
		$(".date_ten").removeClass('act');
		$(".date_twelve").removeClass('act');
	});
	$('.date_twelve').click(function(){ 

		$('#youtube_live').attr('src','https://www.youtube.com/watch?v=Cm4AZzKkItk');
		$(".date_ten").removeClass('act');		
		$(".date_eleven").removeClass('act');
		$(".date_twelve").addClass('act');
		
	});

});
