
document.addEventListener("DOMContentLoaded", function () {
  initialize();
  objectFitImages()
});

var POPUP_COOKIE = 'POPUP_COOKIE';

function handlePopupDontAgainClick(event) {
  var dataset = event.target.dataset;
  if (dataset && dataset.popupKey) {
    setPopupCookie(getPopupCookieKey(dataset.popupKey), true, 1);
    var child = document.querySelector("#" + dataset.popupKey);
    child.parentNode.removeChild(child);
  } else {
    var child = event.target.parentNode.parentNode;
    child.parentNode.removeChild(child);
  }
}

function handlePopupCloseClick(event) {
  var dataset = event.target.dataset;
  if (dataset && dataset.popupKey) {
    var child = document.querySelector("#" + dataset.popupKey);
    child.parentNode.removeChild(child);
  } else {
    var child = event.target.parentNode.parentNode;
    child.parentNode.removeChild(child);
  }
}

function handleMobilePopupDontAgainClick() {
  var mobilePopupElement = document.querySelector('.layer_popup_wrapper.mo_popup');
  setPopupCookie(getPopupCookieKey('MOBILE_POPUP'), true, 1);
  if (mobilePopupElement) mobilePopupElement.parentNode.removeChild(mobilePopupElement);
}

function handleMobilePopupCloseClick() {
  var mobilePopupElement = document.querySelector('.layer_popup_wrapper.mo_popup');
  if (mobilePopupElement) mobilePopupElement.parentNode.removeChild(mobilePopupElement);
}


function initialize() {
  // Desktop
  var popupElements = document.querySelectorAll(".layer_popup_wrapper.pc_popup");

  for (var i = 0; i < popupElements.length; i++) {
    var dataset = popupElements[i].dataset;

    if (dataset && dataset.posY) popupElements[i].style.top = dataset.posY + "px";
    if (dataset && dataset.posX) popupElements[i].style.left = dataset.posX + "px";
    if (dataset && dataset.zIndex) popupElements[i].style.zIndex = dataset.zIndex;

    var popupToday = popupElements[i].querySelector('.popup_today');
    if (popupToday) popupToday.addEventListener('click', handlePopupDontAgainClick);

    var popupClose = popupElements[i].querySelector('.popup_close');
    if (popupClose) popupClose.addEventListener('click', handlePopupCloseClick);
  }

  var cookieList = getAllPopupCookies();

  for (var i = 0; i < cookieList.length; i++) {
    if (cookieList[i]) {
      var idName = "#" + cookieList[i].substring(getPopupCookieKey().length);
      var element = document.querySelector(idName);
      if (element) element.parentNode.removeChild(element);
    }
  }


  var mobilePopupElement = document.querySelector('.layer_popup_wrapper.mo_popup');
  if (mobilePopupElement) {
    var mobilePopupToday = mobilePopupElement.querySelector('.popup_today');
    if (mobilePopupToday) mobilePopupToday.addEventListener('click', handleMobilePopupDontAgainClick);
    var mobilePopupClose = mobilePopupElement.querySelector('.popup_close');
    if (mobilePopupClose) mobilePopupClose.addEventListener('click', handleMobilePopupCloseClick);

  }

  var mobileCookie = getPopupCookie(getPopupCookieKey("MOBILE_POPUP"));
  if (mobileCookie && mobilePopupElement) mobilePopupElement.parentNode.removeChild(mobilePopupElement);

  var mobilePopupContainer = document.querySelector(".layer_popup_wrapper.mo_popup .popup_container");
  if (mobilePopupContainer) hideOnClickOutside(mobilePopupContainer);


}


function getPopupCookieKey(key) {
  var _key = key || ""
  return POPUP_COOKIE + "_" + _key;
}

function setPopupCookie(key, value, day) {
  var d = new Date();
  d.setTime(d.getTime() + (day * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = key + "=" + value + ";" + expires + ";path=/";
}

function getPopupCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getAllPopupCookies() {
  var popupCookieList = []
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(POPUP_COOKIE) == 0) {
      var keyIndex = c.indexOf("=");
      popupCookieList.push(c.substring(0, keyIndex));
    }
  }
  return popupCookieList;
}


function hideOnClickOutside(element) {
  var outsideClickListener = function outsideClickListener(event) {
    if (!element.contains) {
      Node.prototype.contains = document.body.contains
    }
    if (!element.contains(event.target) && isVisible(element)) {
      handleMobilePopupCloseClick();
      removeClickListener();
    }
  };

  var removeClickListener = function removeClickListener() {
    document.removeEventListener('click', outsideClickListener);
  };

  document.addEventListener('click', outsideClickListener);
}

var isVisible = function isVisible(elem) {
  return !!elem && !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
};





var objectFitImages=function(){"use strict";function t(t,e){return"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='"+t+"' height='"+e+"'%3E%3C/svg%3E"}function e(t){if(t.srcset&&!p&&window.picturefill){var e=window.picturefill._;t[e.ns]&&t[e.ns].evaled||e.fillImg(t,{reselect:!0}),t[e.ns].curSrc||(t[e.ns].supported=!1,e.fillImg(t,{reselect:!0})),t.currentSrc=t[e.ns].curSrc||t.src}}function i(t){for(var e,i=getComputedStyle(t).fontFamily,r={};null!==(e=u.exec(i));)r[e[1]]=e[2];return r}function r(e,i,r){var n=t(i||1,r||0);b.call(e,"src")!==n&&h.call(e,"src",n)}function n(t,e){t.naturalWidth?e(t):setTimeout(n,100,t,e)}function c(t){var c=i(t),o=t[l];if(c["object-fit"]=c["object-fit"]||"fill",!o.img){if("fill"===c["object-fit"])return;if(!o.skipTest&&f&&!c["object-position"])return}if(!o.img){o.img=new Image(t.width,t.height),o.img.srcset=b.call(t,"data-ofi-srcset")||t.srcset,o.img.src=b.call(t,"data-ofi-src")||t.src,h.call(t,"data-ofi-src",t.src),t.srcset&&h.call(t,"data-ofi-srcset",t.srcset),r(t,t.naturalWidth||t.width,t.naturalHeight||t.height),t.srcset&&(t.srcset="");try{s(t)}catch(t){window.console&&console.warn("https://bit.ly/ofi-old-browser")}}e(o.img),t.style.backgroundImage='url("'+(o.img.currentSrc||o.img.src).replace(/"/g,'\\"')+'")',t.style.backgroundPosition=c["object-position"]||"center",t.style.backgroundRepeat="no-repeat",t.style.backgroundOrigin="content-box",/scale-down/.test(c["object-fit"])?n(o.img,function(){o.img.naturalWidth>t.width||o.img.naturalHeight>t.height?t.style.backgroundSize="contain":t.style.backgroundSize="auto"}):t.style.backgroundSize=c["object-fit"].replace("none","auto").replace("fill","100% 100%"),n(o.img,function(e){r(t,e.naturalWidth,e.naturalHeight)})}function s(t){var e={get:function(e){return t[l].img[e?e:"src"]},set:function(e,i){return t[l].img[i?i:"src"]=e,h.call(t,"data-ofi-"+i,e),c(t),e}};Object.defineProperty(t,"src",e),Object.defineProperty(t,"currentSrc",{get:function(){return e.get("currentSrc")}}),Object.defineProperty(t,"srcset",{get:function(){return e.get("srcset")},set:function(t){return e.set(t,"srcset")}})}function o(){function t(t,e){return t[l]&&t[l].img&&("src"===e||"srcset"===e)?t[l].img:t}d||(HTMLImageElement.prototype.getAttribute=function(e){return b.call(t(this,e),e)},HTMLImageElement.prototype.setAttribute=function(e,i){return h.call(t(this,e),e,String(i))})}function a(t,e){var i=!y&&!t;if(e=e||{},t=t||"img",d&&!e.skipTest||!m)return!1;"img"===t?t=document.getElementsByTagName("img"):"string"==typeof t?t=document.querySelectorAll(t):"length"in t||(t=[t]);for(var r=0;r<t.length;r++)t[r][l]=t[r][l]||{skipTest:e.skipTest},c(t[r]);i&&(document.body.addEventListener("load",function(t){"IMG"===t.target.tagName&&a(t.target,{skipTest:e.skipTest})},!0),y=!0,t="img"),e.watchMQ&&window.addEventListener("resize",a.bind(null,t,{skipTest:e.skipTest}))}var l="fregante:object-fit-images",u=/(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g,g="undefined"==typeof Image?{style:{"object-position":1}}:new Image,f="object-fit"in g.style,d="object-position"in g.style,m="background-size"in g.style,p="string"==typeof g.currentSrc,b=g.getAttribute,h=g.setAttribute,y=!1;return a.supportsObjectFit=f,a.supportsObjectPosition=d,o(),a}();
