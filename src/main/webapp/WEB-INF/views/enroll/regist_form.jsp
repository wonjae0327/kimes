<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-05
  Time: 오전 9:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="container">
    <div class="sub_top enroll">
        <div class="lnb_wrap">
            <ul class="lnb">
                <li><a href="#"><span>참관사전등록</span></a></li>
                <li><a href="#"><span>사전등록 조회</span></a></li>
            </ul>
            <span class="bg"></span>
        </div>
    </div>

    <div id="content" class="content">
        <!-- top -->
        <div class="location">
            <a href="<c:url value='/' />">HOME</a> &gt; 참관사전등록 &gt; 참관 사전등록
        </div>
        <div class="top_tit">
            <h2>참관 사전등록</h2>
        </div>
        <!--// top -->

        <!-- tab -->
        <div class="tab">
            <ul>
                <li class="on">
                    <a href="<c:url value='/enroll/regist_form' />"><span>일반 사전등록</span></a>
                </li>
                <li>
                    <a href="#"><span>단체 사전등록</span></a>
                </li>
            </ul>
        </div>
        <!-- tab -->

        <div class="toptxt bdr">
            정부 방역지침에 따라 <strong class="f_red01">본인 명의로 1명만 등록 및 입장이 가능하며, 1인명의로 다수 입장이 불가</strong>하오니 지침을 준수하여 주시기 바랍니다.<br/>
            전시회 관람은 <strong class="f_red01">대학생 이상</strong>만 가능합니다. (미성년자는 보호자 동반 입장시 관람 가능합니다)

            <c:set var="now" value="<%=new java.util.Date()%>"/>
            <fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowD"/>
            <fmt:parseDate value="${nowD}" pattern="yyyy-MM-dd HH:mm:ss" var="nowfmtTime"/>
            <fmt:parseDate var="limit" pattern="yyyy-MM-dd HH:mm:ss" value="2024-02-29 23:59:00"/>

            <c:if test="${(limit.time - now.time) > 0}">
                <br>
                <br>
                - 3월 1일 ~ 3월 13일까지 온라인/모바일 사전등록 시 입장료 10,000원 <Br>
                - 3월 14일 ~ 3월 17일까지 현장결제 시 입장료 20,000원이 발생합니다.<Br>
                - 입장료 결제는 전시회 현장 유인/무인등록데스크에서 결제 진행 후 출입증을 수령해주시기 바랍니다.<Br><Br>
                <script>
                    alert("3월 1일 ~ 3월 13일까지 온라인/모바일 사전등록 시 입장료 10,000원/3월 14일 ~ 3월 17일까지 현장결제 시 입장료 20,000원이 발생합니다. 입장료 결제는 전시회 현장 유인/무인등록데스크에서 결제 진행 후 출입증을 수령해주시기 바랍니다.");
                </script>
            </c:if>

            <div class="ad_banner mt10 mb10">
                <ul>
                    <li>
                        <a href="https://www.modinex.co.kr/" target="_blank">
                            <img src="<c:url value='/asset/images/enroll/kimes2024_regist_img01_1.png' />">
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <h3>기본정보 입력</h3>

        <form id="frm" name="frm" method="post" action="<c:url value='/enroll/regist_form_ok' />" target="ifm_hidden"
              enctype="multipart/form-data">
            <input type="hidden" name="mode" value="">
            <input type="hidden" name="syear" value="">
            <input type="hidden" name="lang" value="">
            <input type="hidden" name="param" value="">
            <input type="hidden" name="idx" value="">
            <input type="hidden" name="chk_regist" value="Y">
            <input type="hidden" name="site" value="">
            <input type="hidden" name="value" value="normal">

            <!-- 기본정보 입력 -->
            <div class="board_write">
                <table>
                    <colgroup>
                        <col style="width: 16%;">
                        <col style="width: 34%;">
                        <col style="width: 16%;">
                        <col style="width: 34%;">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>성명<span class="required">*</span></th>
                        <td colspan="3">
                            <input type="text" id="user_name_kor" name="user_name_kor" maxlength="100" value=""
                                   onblur="user_name_kor_blur();"/>
                        </td>
                    </tr>
                    <tr>
                        <th>연령<span class="required">*</span></th>
                        <td colspan="3">
                            <div class="inp_chk">
                                <input type="radio" name="birth" id="birth_A" value="A"><label for="birth_A">10대</label>
                                <input type="radio" name="birth" id="birth_B" value="B"><label for="birth_B">
                                20대</label>
                                <input type="radio" name="birth" id="birth_C" value="C"><label for="birth_C">
                                30대</label>
                                <input type="radio" name="birth" id="birth_D" value="D"><label for="birth_D">
                                40대</label>
                                <input type="radio" name="birth" id="birth_E" value="E"><label for="birth_E">
                                50대</label>
                                <input type="radio" name="birth" id="birth_F" value="F"><label for="birth_F">
                                60대</label>
                                <input type="radio" name="birth" id="birth_G" value="G"><label for="birth_G">
                                70대</label>
                                <input type="radio" name="birth" id="birth_H" value="H"><label for="birth_H">
                                80대</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>성별<span class="required">*</span></th>
                        <td>
                            <div class="inp_chk">
                                <input type="radio" name="sex" id="sex_A" value="남"> <label for="sex_A">남자</label>
                                <input type="radio" name="sex" id="sex_B" value="여"> <label for="sex_B">여자</label>
                            </div>
                        </td>
                        <th class="line">구분<span class="required">*</span></th>
                        <td>
                            <div class="inp_chk">
                                <input type="radio" name="VISIT_TYPE" id="VISIT_TYPE_Y" value="Y"
                                       onclick="noment()"><label for="VISIT_TYPE_Y">비즈니스</label>
                                <input type="radio" name="VISIT_TYPE" id="VISIT_TYPE_N" value="N" onclick="noment()">
                                <label for="VISIT_TYPE_N">일반</label>
                            </div>
                            <div id="meeting" class="f_small f_red01 mt5"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>회사명(소속)<span class="required">*</span></th>
                        <td colspan="3">
                            <div class="inp_aside">
                                <input type="text" id="company_name_kor" name="company_name_kor" maxlength="100"
                                       value="" onblur="company_name_kor_blur();"/>
                                <div class="aside">
                                    <input type="checkbox" name="NC" id="NC" value="Y">
                                    <label for="NC">소속없음</label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>부서</th>
                        <td>
                            <input type="text" name="part_kor" id="part_kor" maxlength="100" value="">
                        </td>
                        <th class="line">직위</th>
                        <td>
                            <input type="text" name="position_kor" id="position_kor" maxlength="100" value="">
                        </td>
                    </tr>
                    <tr>
                        <th>주소<span class="required">*</span></th>
                        <td colspan="3">
                            <div class="inp_addr">
                                <div class="post">
                                    <input type="text" name="zipcode" id="zipcode" maxlength="5"
                                           value="" onclick="sample6_execDaumPostcode()"
                                           readonly>
                                    <a href="javascript:void(0);" onclick="sample6_execDaumPostcode()"
                                       class="btn mid"><span>우편번호 찾기</span></a>
                                </div>
                                <div class="addr">
                                    <input type="text" name="addr1" id="addr1" maxlength="200" value="" readonly>
                                    <input type="text" name="addr2" id="addr2" maxlength="200" value=""
                                           placeholder="상세주소">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>휴대전화<span class="required">*</span></th>
                        <td colspan="3">
                            <div class="inp_aside">
                                <input type="text" id="mobile" name="mobile" placeholder="-없이 숫자 입력"
                                       value="" maxlength="20" numberonly="true">
                                <div class="aside inp_chk">
                                    <input type="radio" id="chk_smsY" name="mbzine" value="Y">
                                    <label for="chk_smsY">수신 동의 <span class="f_11px f_gray01">(전시회 소식, 경품, 이벤트 등)</span></label>
                                    <input type="radio" id="chk_smsN" name="mbzine" value="N">
                                    <label for="chk_smsN">수신 거부</label>
                                </div>
                                <div class="f_red01 f_14px mt10">※ 카카오톡으로 출입용 바코드가 발송되오니 오타없이 입력 부탁드립니다.</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>이메일<span class="required">*</span></th>
                        <td colspan="3">
                            <div class="inp_aside">
                                <input type="text" id="email" name="email" value="" maxlength="50">
                                <div class="aside inp_chk">
                                    <input type="radio" id="chk_emailY" name="webzine" value="Y">
                                    <label for="chk_emailY">수신 동의 <span
                                            class="f_11px f_gray01">(전시회 소식, 경품, 이벤트 등)</span></label>
                                    <input type="radio" id="chk_emailN" name="webzine" value="N">
                                    <label for="chk_emailN">수신 거부</label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>홈페이지</th>
                        <td colspan="3">
                            <div class="inp_aside website">
                                <span>http://</span><input type="text" id="homepage" name="homepage" value=""
                                                           maxlength="200">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- 기본정보 입력 -->

            <!-- 기타 입력사항 -->
            <div class="tit_area gap">
                <h3>기타 입력사항</h3>
                <div class="add">
                    <span class="required">*</span> 필수 입력사항입니다. 하나 이상 체크해 주세요.
                </div>
            </div>

            <div class="board_write">
                <table>
                    <colgroup>
                        <col style="width: 11%;">
                        <col style="width: 89%;">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>직업분류<span class="required">*</span></th>
                        <td>
                            <ul class="inp_chk_list">
                                <c:forEach var="item" items="${codes}" varStatus="i">
                                    <c:if test="${item.CD_TYPE eq 'vopt_01_data' }">
                                        <li>
                                            <input type="radio" id="opt_01_${i.index}" name="opt_01"
                                                   value="${item.CD_CODE}">
                                            <label for="opt_01_${i.index}">${item.CD_VALUE_KR}</label>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>관심분야<span class="required">*</span></th>
                        <td>
                            <ul class="inp_chk_list">
                                <!-- asp상 하드 코딩으로 되어 있는 데이터라 vue.js로 구현 -->
                                <li v-for="(value, index) in interest" :key="index">
                                    <input type="checkbox" :id="'opt_03_' + ++index" name="opt_03"
                                           :value="value.value">
                                    <label :for="'opt_03_' + index">{{value.text}}</label>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>참관목적<span class="required">*</span></th>
                        <td>
                            <ul class="inp_chk_list">
                                <!-- asp상 하드 코딩으로 되어 있는 데이터라 vue.js로 구현 -->
                                <li v-for="(value, index) in purpose" :key="index">
                                    <input type="checkbox" :id="'opt_02_' + ++index" name="opt_02"
                                           :value="value.value">
                                    <label :for="'opt_02_' + index">{{value.text}}</label>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>참관경로<span class="required">*</span></th>
                        <td>
                            <ul class="inp_chk_list">
                                <c:forEach var="item" items="${codes}" varStatus="i">
                                    <c:if test="${item.CD_TYPE eq 'vopt_04_data' }">
                                        <li>
                                            <input type="checkbox" id="opt_04_${i.index}" name="opt_04"
                                                   value="${item.CD_CODE}">
                                            <label for="opt_04_${i.index}">${item.CD_VALUE_KR}</label>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>참관예정일<span class="required">*</span></th>
                        <td>
                            <ul class="inp_chk_list">
                                <li>
                                    <input type="checkbox" id="chk_date01" name="opt_07" value="A"
                                           onclick="oneCheckbox(this)">
                                    <label for="chk_date01">3월 14일(목)</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="chk_date02" name="opt_07" value="B"
                                           onclick="oneCheckbox(this)">
                                    <label for="chk_date02">3월 15일(금)</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="chk_date03" name="opt_07" value="C"
                                           onclick="oneCheckbox(this)">
                                    <label for="chk_date03">3월 16일(토)</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="chk_date04" name="opt_07" value="D"
                                           onclick="oneCheckbox(this)">
                                    <label for="chk_date04">3월 17일(일)</label>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- 기타 입력사항 -->

            <!-- 개인정보 수집·이용 동의서 -->
            <h3 class="gap">전시회 관람 및 전자명함 출입증 발급을 위한 개인정보 수집·이용 동의서</h3>
            <!-- agree -->
            <div class="agree_wrap">
                <div class="agree_cont">
                    <p class="top">본 전시회와 관련하여 아래와 같이 개인정보를 수집·이용하고자 합니다. 내용을 자세히 읽으신 후 동의 여부를 결정하여 주십시오.</p>

                    <h4>개인정보 수집 항목</h4>
                    <h5>참관객 식별 및 출입증 발급</h5>
                    <ul class="ul_type01">
                        <li>수집항목 : 성명, 소속명, 핸드폰, 또는 이메일</li>
                        <li>수집근거 : 개인정보 보호법 제 15조 제 1항</li>
                        <li>보유기간 : 2년</li>
                    </ul>

                    <h5>전시회 안내 및 고지사항 전달</h5>
                    <ul class="ul_type01">
                        <li>수집항목 : 휴대폰, 이메일</li>
                        <li>수집근거 : 개인정보 보호법 제 15조 제 1항</li>
                        <li>보유기간 : 2년</li>
                    </ul>

                    <h5>전시회 인증용 분석 자료</h5>
                    <ul class="ul_type01">
                        <li>수집항목 : 성명, 소속, 핸드폰, 이메일, 설문조사 항목</li>
                        <li>수집근거 : 전시산업발전법 제 22조</li>
                        <li>보유기간 : 2년</li>
                    </ul>

                    <p class="mid">단, 개인정보 보호법 제15조 제1항 제3호에 의거하여 ‘전시회 인증용 분석 자료’ 목적의 해당 수집 항목은 정보의 주체 동의 없이 이용될 수 있음을
                        고지합니다.</p>

                    <h4>개인정보 취급 업무의 위탁</h4>
                    <p class="mid">본 전시회 서비스 향상을 위하여 아래와 같이 개인정보를 등록시스템 전문 업체에 위탁하고 있으며, 관계 법령에 따라 위탁계약 시 개인정보가 안전하게 관리될
                        수 있도록 필요한 사항을 규정하고 있습니다.</p>
                    <ul class="ul_type01">
                        <li>수탁업체 : (주)시스포유</li>
                        <li>위탁 업무내용 : 등록시스템 운영위탁, 전자명함 시스템 서비스 제공 등</li>
                        <li>이용 및 보유기간 : 위탁계약 종료시까지</li>
                    </ul>
                    <p class="mid">위의 개인정보 수집·이용에 대한 동의를 거부할 권리가 있습니다. 그러나 동의를 거부할 경우, 본 전시회가 제공하는 서비스를 이용할 수 없습니다. 또한
                        전시회 출입증 발급이 불가하여 재입장시 입장 제한이 발생될 수 있습니다.</p>

                    <h4>홍보 및 마케팅에 관한 동의</h4>
                    <p class="mid">본 전시회는 “개인정보 보호법”에 따라 동의를 얻어 아래와 같이 한국이앤엑스에서 주최하는 차기 전시회 홍보 및 마케팅을 위한 개인정보를 수집·이용
                        합니다.</p>

                    <ul class="ul_type01">
                        <li>수집목적 : 차기년도 전시회 홍보 및 무료초청장 발송 (이메일 : 성명, 이메일 / 모바일 : 성명, 휴대폰)</li>
                        <li>보유기간 : 2년</li>
                    </ul>

                    <h4>전자명함시스템에 의한 개인정보 제3자 제공 동의</h4>
                    <p class="mid">본 전시회는 참관객과 참가업체 상호간의 유용한 정보교류 서비스 제공을 위한 전자명함시스템을 운용 합니다.</p>
                    <p class="mid">※ 전시장내에서 참가업체 부스 방문 시 출입증에 표기된 바코드를 스캔(사용)하는 경우, 이는 상호간에 명함교환 행위와 동일한 것으로 간주하여 해당
                        참가업체에게 고객님의 개인정보가 자동으로 전달됩니다.</p>
                    <ul class="ul_type01">
                        <li>제공받는 자 : 전자명함(고객관리) 시스템 사용 참가업체</li>
                        <li>제공목적 : 비즈니스&마케팅 활용</li>
                        <li>제공항목 : 성명, 소속, 부서, 직위 등 관람객 정보 및 설문조사 항목</li>
                        <li>보유기간 : 1년</li>
                        <li>제공여부 : 출입증 바코드 스캔</li>
                    </ul>
                </div>
            </div>
            <!-- agree -->
            <!-- 개인정보 수집·이용 동의서 -->

            <div class="agree_chk">
                <input type="checkbox" id="chk_agree" name="chk_agree"><label for="chk_agree">개인정보 수집·이용 및 활용에 대해
                동의합니다.</label>
            </div>

            <div class="bx_type04 mt30 mb10 f_17px" style="text-align:center;">
                <strong>
                    <span class="f_red01">방문자 본인 정보</span>를 정확히 기재해주시기 바랍니다.<br/>
                    <span class="f_red01">동일한 정보로 다수 입력시 입장이 불가</span>하오니 유의하시기 바랍니다.
                </strong>
            </div>

            <fmt:parseDate value="${code.VST_edate += ' ' += code.VST_etime}" pattern="yyyy-MM-dd HH:mm:ss"
                           var="endDate"/>

            <c:choose>
                <c:when test="${nowfmtTime.time < endDate.time}">
                    <p style="text-align: center;"><strong class="f_red01">사전등록이 마감되었습니다.</strong></p>
                </c:when>
                <c:otherwise>
                    <div class="btn_area" id="btn_area">
                        <a class="btn large" onclick="pre_regist();">
                            <span>${mode eq 'mod' ? "수정하기" : "신청하기"}</span>
                        </a>
                    </div>
                </c:otherwise>
            </c:choose>
        </form>

    </div>
</div>
