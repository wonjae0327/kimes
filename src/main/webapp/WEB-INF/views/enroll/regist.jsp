<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-04
  Time: 오후 1:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="container">
    <div class="sub_top enroll">
        <div class="lnb_wrap">
            <ul class="lnb">
                <li><a href="#"><span>참관사전등록</span></a></li>
                <li><a href="#"><span>사전등록 조회</span></a></li>
            </ul>
            <span class="bg"></span>
        </div>
    </div>

    <div id="content" class="content">
        <!-- top -->
        <div class="location">
            <a href="<c:url value='/' />">HOME</a> &gt; 참관사전등록 &gt; 참관 사전등록
        </div>
        <div class="top_tit">
            <h2>참관 사전등록</h2>
        </div>
        <!--// top -->

        <ul class="bx_type05 mb50">
            <li><img src="<c:url value='/asset/images/enroll/2022img.png' />"></li>
            <li>
                <p class="mb20">
                    <!--정부 방역지침에따라-->
                    <strong class="f_red01">본인 명의로 1명만 등록 및 입장이 가능하며, 1인명의로 다수 입장이 불가</strong>하오니 지침을 준수하여 주시기
                    바랍니다.<br/><br/></strong>
                    전시회 관람은 <strong class="f_red01">대학생 이상</strong>만 가능합니다. (미성년자는 보호자 동반 입장시 관람 가능합니다)
                </p>
            </li>
        </ul>

        <div class="exhibit_top mb50">
            <c:set var="now" value="<%=new java.util.Date()%>"/>
            <fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowD"/>
            <fmt:parseDate value="${nowD}" pattern="yyyy-MM-dd HH:mm:ss" var="nowfmtTime"/>
            <fmt:parseDate value="${code.VST_edate += ' ' += code.VST_etime }" pattern="yyyy-MM-dd HH:mm:ss"
                           var="endDate"/>
            <div class="btn_area">
                <c:choose>
                    <c:when test="${nowfmtTime.time < endDate.time}">
                        <a href="<c:url value='/enroll/regist_form' />" class="btn large02 btn_re">KIMES 2024 사전등록</a>
                    </c:when>
                    <c:otherwise>
                        <!--<a href="javascript:alert('마감되었습니다.');" class="btn large02 btn_re">KIMES 2024 사전등록</a>-->
                    </c:otherwise>
                </c:choose>

                <a href="<c:url value='/enroll/regist_form' />" class="btn large02 btn_re">KIMES 2024 사전등록</a>
            </div>

            <div class="tbl tac mt40">
                <table>
                    <colgroup>
                        <col style="width:15%;">
                        <col style="width:20%;">
                        <col style="width:20%;">
                        <col style="width:20%;">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>사전등록<br>기간</th>
                        <th>2023년 11월 21일<BR> ~ <BR>2024년 2월 29일</th>
                        <th>2024년 3월 1일<BR> ~ <BR>2024년 3월 13일</th>
                        <th>현장 티켓 구매 <BR> 2024년 3월 14일 ~ 17일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="background:#eeeff4;">입장료</td>
                        <td>무료 입장</td>
                        <td>10,000원</td>
                        <td>20,000원</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <img src="<c:url value='/asset/images/enroll/procedure_img.png' />">

        </div>

    </div>
</div>