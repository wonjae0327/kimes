<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-01
  Time: 오후 1:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:formatDate var="today" value="<%= new java.util.Date() %>" pattern="yyyy-MM-dd"/>
<div id="main_visual_wrap">
    <div class="main_visual">
        <div class="video_wrap">
            <iframe src="https://player.vimeo.com/video/843740292?autoplay=1&loop=1&title=0&background=1"
                    frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen
                    allowfullscreen></iframe>

            <div class="visual_type">
                <img src="<c:url value='/main_images/main/visual_typo.png' />">
                <p>국제의료기기&amp;병원설비전시회</p>
                <div class="date">2024. 3. 14<span>(Thu)</span> - 17<span>(Sun)</span><strong>COEX, SEOUL KOREA</strong>
                </div>
            </div>
        </div>
        <div>
            <div class="quickmenu_wrap">
                <!--div class="col left">
                    <div class="quick_row">
                        <a href="/kor/visit/exhibit.asp"><p>참가신청</p></a>
                        <span><a href="/kor/visit/exhibit.asp">바로가기 <i class="xi-angle-right-min"></i></a></span>
                    </div>
                </div-->
                <div class="col left">
                    <div class="quick_row">
                        <a href="/kor/search/product.asp"><p>참가업체 검색</p>
                            <!--<strong>KIMES 참가업체를 한눈에 보실 수 있습니다.</strong>--></a>
                        <span><a href="/kor/search/product.asp">바로가기 <i class="xi-angle-right-min"></i></a></span>
                    </div>
                </div>
                <div class="col center">
                    <div class="quick_row">
                        <a href="/kor/enroll/regist.asp"><p>사전등록</p>
                            <!--<strong>사전 등록 하시면 더욱 편히 전시회를 즐기실 수 있습니다.</strong>--></a>
                        <span><a href="/kor/enroll/regist.asp">바로가기 <i class="xi-angle-right-min"></i></a></span>
                    </div>
                </div>

                <!--<div class="col center">
                    <div class="quick_row">
                            <a href="/kor/enroll/regist_search.asp"><p>사전등록 조회</p>
                                                                             <strong>성함과 이메일로 사전등록 내역을 확인하실 수 있습니다.</strong></a>
                            <span><a href="/kor/enroll/regist_search.asp">바로가기 <i class="xi-angle-right-min"></i></a></span>
                        </div>
                </div>-->

                <div class="col right">
                    <div class="quick_row">
                        <a href="/kor/search/floor.asp"><p>전시장 도면</p>
                            <!--<strong>의료분야 전문가들이 한자리에 모이는 글로벌 지식 공유 플랫폼<br/>
KIMES 2023 기간 동안 약 200개의 주제로 컨퍼런스 및 학술대회가 동시 개최됩니다.</strong>--></a>
                        <span><a href="/kor/search/floor.asp">바로가기 <i class="xi-angle-right-min"></i></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 공지사항 게시판 추출 //--> <!-- 샐 -->
<section id="notice_bbs" class="main_section mb120" style="display:;">
    <div class="colum_content_wrap">

        <dl class="colum_item">
            <dt>
                <div class="title">KIMES NEWS</div>
                <div class="btn_category_wrap">
                    <a class="act">공지사항</a>
                    <a class="notice_bbs_btn">지원사업 안내</a>
                </div>
                <div class="btn_more">
                    <a href="/kor/board/notice_list.asp?site_type=G"><i class="xi-plus"></i></a>
                </div>
            </dt>

            <dd>
                <div>
                    <ol>
                        <c:if test="${not empty notices2 }">
                            <c:forEach var="notice2" items="${notices2 }">
                                <li>
                                    <a href="<c:url value='#' />">
                                        <dl class="bbs_item">
                                            <dt>
                                                <c:if test="${fn:substring(notice2.B_WRITEDAY, 0, 10) eq today }">
                                                    <span class="ico_new">N</span>
                                                </c:if>
                                                <strong>${fn:substring(notice2.B_SUBJECT, 0, 80)}</strong>
                                                <p>${fn:replace(fn:substring(notice2.B_WRITEDAY, 0, 10), "-", ".")}</p>
                                            </dt>
                                        </dl>
                                    </a>
                                </li>
                            </c:forEach>
                        </c:if>

                        <c:if test="${not empty notices }">
                            <c:forEach var="notice" items="${notices }">
                                <li>
                                    <a href="#">
                                        <dl class="bbs_item">
                                            <dt>
                                                <c:if test="${fn:substring(notice.B_WRITEDAY, 0, 10) eq today }">
                                                    <span class="ico_new">N</span>
                                                </c:if>
                                                <strong>${fn:substring(notice.B_SUBJECT, 0, 80) }</strong>
                                                <p>${fn:replace(fn:substring(notice.B_WRITEDAY, 0, 10), "-", ".")}</p>
                                            </dt>
                                        </dl>
                                    </a>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ol>
                </div>
            </dd>
        </dl>

        <dl style="padding-top: 50px;">
            <div class="btn_area">
                <a href="#" target="_blank" class="btn large02 btn_re" style="color: #fff;">
                    <span> E-Book 바로가기 국문</span>
                </a>
            </div>
            <div class="btn_area">
                <a href="#" target="_blank" class="btn large02 btn_re" style="color:#fff;">
                    <span> E-Book 바로가기 영문</span>
                </a>
            </div>
        </dl>
    </div>
</section>

<!-- 지원산업안내 게시판 추출 우선 비노출로 //-->
<section id="project_bbs" class="main_section mb120" style="display: none;">
    <div class="colum_content_wrap">

        <dl class="colum_item">
            <dt>
                <div class="title">KIMES NEWS</div>
                <div class="btn_category_wrap">
                    <a class="project_bbs_btn">공지사항</a>
                    <a class="act">지원사업 안내</a>
                </div>
                <div class="btn_more">
                    <a href="#"><i class="xi-plus"></i></a>
                </div>
            </dt>

            <dd>
                <div>
                    <ol>
                        <c:if test="${not empty notices1 }">
                            <c:forEach var="notice1" items="${notices1 }">
                                <li>
                                    <a href="#">
                                        <dl class="bbs_item">
                                            <dt>
                                                <c:if test="${fn:substring(notice1.B_WRITEDAY, 0, 10) eq today }">
                                                    <span class="ico_new">N</span>
                                                </c:if>
                                                <strong>${fn:substring(notice1.B_SUBJECT, 0, 80) }</strong>
                                                <p>${fn:replace(fn:substring(notice1.B_WRITEDAY, 0, 10), "-", ".")}</p>
                                            </dt>
                                        </dl>
                                    </a>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ol>
                </div>
            </dd>
        </dl>
    </div>
</section>

<!-- 도면 안내 바로가기 메뉴 //-->
<section class="main_section">
    <div class="colum_content_wrap planpaper_wrap">

        <dl class="colum_item planpaper">
            <dt>
                <div class="title">도면 안내</div>
                <span>전시장 버튼을 클릭하시면<br> 도면을 자세히 보실 수 있습니다.</span>
            </dt>

            <dd>
                <ul class="planpaper_list">
                    <li>
                        <a href="#" target="_blank">전체도면<p><i class="xi-plus"></i></p></a>
                    </li>
                    <li>
                        <a href="#" target="_blank">HALL A<p><i class="xi-plus"></i></p></a>
                    </li>
                    <li>
                        <a href="#" target="_blank">HALL B<p><i class="xi-plus"></i></p></a>
                    </li>
                    <li>
                        <a href="#" target="_blank">HALL C<p><i class="xi-plus"></i></p></a>
                    </li>
                    <li>
                        <a href="#" target="_blank">HALL D<p><i class="xi-plus"></i></p></a>
                    </li>
                    <li>
                        <a href="#" target="_blank">HALL E<p><i class="xi-plus"></i></p></a>
                    </li>
                </ul>
            </dd>
        </dl>
    </div>
</section>

<!-- 라이브 3월 10일 //-->
<section class="main_section live_wrap">
    <dl class="live_item">
        <dt>
            <div class="title_live">KIMES 2024 라이브</div>
            <div class="title_live_small"></div>
            <div class="btn_category_wrap">
                <a href="javascript:void(0);" onclick="day_sel('A')">Day 1</a>
                <a href="javascript:void(0);" onclick="day_sel('B')">Day 2</a>
                <a href="javascript:void(0);" onclick="day_sel('C')">Day 3</a>
            </div>
        </dt>
        <dd id="day1" style="display: ;">
            <div class="video">
                <div class="video-container">
                    <iframe width="100%" height="100%" id="youtube_live" src="https://www.youtube.com/embed/4T1v8pIeWXY"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
            </div>
        </dd>
        <dd id="day2" style="display:none;">
            <div class="video">
                <div class="video-container">
                    <iframe width="100%" height="100%" id="youtube_live" src="https://www.youtube.com/embed/AGkrGfm6KHo"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
            </div>
        </dd>
        <dd id="day3" style="display:none;">
            <div class="video">
                <div class="video-container">
                    <iframe width="100%" height="100%" id="youtube_live" src="https://www.youtube.com/embed/Cm4AZzKkItk"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
            </div>
        </dd>
    </dl>
</section>