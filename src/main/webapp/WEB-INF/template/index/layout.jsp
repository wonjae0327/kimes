<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-01
  Time: 오후 2:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="height=device-height, width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta property="og:image" content="<c:url value='/asset/images/common/meta_img_2024_2.jpg' />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>KIMES 2024 제39회 국제의료기기병원설비 전시회</title>

    <!-- stylesheet -->
    <link rel="stylesheet" href="<c:url value='/main_common/css/reset.css' />">
    <link rel="stylesheet" href="<c:url value='/main_common/css/layout.css' />">
    <link rel="stylesheet" href="<c:url value='/main_common/css/font.css' />">
    <link rel="stylesheet" href="<c:url value='/main_common/css/form.css' />">
    <link rel="stylesheet" href="<c:url value='/main_common/css/main.css' />">
    <link rel="stylesheet" href="<c:url value='/main_common/css/swiper.min.css' />">
    <!-- stylesheet -->

    <!-- javascript -->
    <!--<script src='<c:url value="/webjars/jquery/3.7.1/dist/jquery.min.js" />'></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="<c:url value='/main_common/js/aos.js' />"></script>
    <script src="<c:url value='/main_common/js/swiper.js' />"></script>
    <script src="<c:url value='/main_common/js/modalEffects.js' />"></script>
    <script src="<c:url value='/main_common/js/popup.js' />"></script>
    <script src="<c:url value='/main_common/js/common.js' />"></script>
    <!-- javascript -->

    <!-- Meta Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-226008458-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-226008458-1');
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV5RZ87"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '272971297758190');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=272971297758190&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Meta Pixel Code -->

    <!-- 개별 스타일+스크립트 //-->

    <!-- style -->
    <style>
        .menu_bg {
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            position: absolute;
            top: 0;
            display: none;
            z-index: 9998;
        }

        .sidebar_menu {
            display: none;
            width: 50%;
            height: 100%;
            background: #e60012;
            position: absolute;
            top: 0;
            right: -50%;
            z-index: 9999;
        }

        .close_btn {
            width: 50px;
            height: 50px;
            margin: 10px;
        }

        .close_btn > a {
            display: block;
            width: 100%;
            height: 100%;
            font-size: 30px;
            color: #fff;
        }

        .menu_wrap {
            list-style: none;
            padding: 20px 20px;
        }

        .menu_wrap li a {
            color: #fff;
            text-decoration: none;
        }

        .accordion-box {
            width: 100%;
            height: 80vh;
            overflow-y: scroll;
            margin: 0 auto;
            padding-bottom: 50px;
        }

        p.title {
            width: 80%;
            margin: 0 10%;
            line-height: 50px;
            font-size: 20px;
            background: none;
            color: rgba(255, 255, 255, 0.7);
            box-sizing: border-box;
            padding: 0 10px;
        }

        p.title.act {
            border-bottom: 1px solid rgba(255, 255, 255, 0.7);
            color: rgba(255, 255, 255, 1);
        }

        .mobile_submenu {
            padding: 10px 10% 30px;
            display: none;
        }

        .mobile_submenu a {
            display: inline-block;
            margin: 3px 8px;
            color: rgba(255, 255, 255, 0.7);
            font-size: 20px;
            text-align: center;
        }


        @media screen and (max-width: 767px) {
            .sidebar_menu {
                width: 80%;
            }

            p.title {
                margin: 0 10%;
                line-height: 50px;
                font-size: 24px;
            }

            .mobile_submenu a {
                margin: 3px 10px;
                font-size: 18px;
            }
        }

        /* 스와이퍼 기본 디자인 조정 */
        .swiper-container {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            background: #fff;
        }

        .swiper-container-horizontal > .swiper-pagination-bullets {
            bottom: 0px;
            left: 40px;
        }

        .swiper-pagination {
            position: absolute;
            text-align: left;
        }

        .swiper-pagination-bullet {
            width: 12px;
            height: 12px;
        }

        .swiper-pagination-bullet-active {
            opacity: 1;
            background: #e60012;
        }

        @media screen and (max-width: 1023px) {
            .swiper-container-horizontal > .swiper-pagination-bullets {
                left: 0px;
            }

            .swiper-pagination {
                position: absolute;
                text-align: center;
            }
        }

        .btn_area {
            padding: 10px 50px;
            display: inline-block;
            background: #e60012;
            border-radius: 50px;
            font-size: 20px;
        }

        .btn_area:nth-child(2) {
            margin-left: 50px;
        }

        @media screen and (max-width: 630px) {
            .btn_area:nth-child(2) {
                margin-left: 0px;
                margin-top: 20px;
            }
        }

        @media screen and (max-width: 350px) {
            .btn_area {
                padding: 10px 30px;
                border-radius: 50px;
                font-size: 15px;
            }
        }
    </style>
    <!-- style -->

</head>
<body>

<div id="body_wrap">

    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="body"/>
    <tiles:insertAttribute name="footer"/>

</div>
<script>
    function day_sel(value1) {
        if (value1 == "A") {

            $("#day1").show();
            $("#day2").hide();
            $("#day3").hide();
        }
        if (value1 == "B") {

            $("#day1").hide();
            $("#day2").show();
            $("#day3").hide();
        }
        if (value1 == "C") {

            $("#day1").hide();
            $("#day2").hide();
            $("#day3").show();
        }
    }
</script>
</body>
</html>
