<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-01
  Time: 오후 4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<script>
    $(function () {
        $('.menu_btn').on('click', function () {
            $('html, body').css({'overflow': 'hidden', 'height': '100%'}); // 모달팝업 중 html,body의 scroll을 hidden시킴
            $('.menu_bg').show();
            $('.sidebar_menu').show().animate({
                right: 0
            });
        });

        $('.close_btn>a').on('click', function () {
            $('html, body').css({'overflow': 'auto', 'height': '100%'});
            $('.menu_bg').hide();
            $('.sidebar_menu').animate({
                right: '-' + 50 + '%'
            }, function () {
                $('.sidebar_menu').hide();
            });
        });
        
        $(".nav_top .hd_inner").on("mouseenter", function() {
			if (!$('.hd_btm').hasClass('down') &&
				!$('.hd_btm').hasClass('up')) {
				$('.hd_btm').addClass('down').slideDown(200, function() {
					$(this).removeClass('down');
				});
			}
			$("header").addClass('active');
			/*$('.hd_btm').css('opacity','1');*/

		});
		$("header").on("mouseleave", function() {
			if (!$('.hd_btm').hasClass('up')) {
				$('.hd_btm').addClass('up').slideUp(200, function() {
					$(this).removeClass('up');
				});
			}
			$("header").removeClass('active');
			$(".nav_top li").removeClass('hover');
			$(".hd_btm li").removeClass('active');
			/*$('.hd_btm').css('opacity','0');*/
		});


		$(".nav_top li:nth-child(1)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(1)").addClass("active");
		});
		$(".hd_btm li:nth-child(1)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(1)").addClass("hover");
		});
		$(".nav_top li:nth-child(2)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(2)").addClass("active");
		});
		$(".hd_btm li:nth-child(2)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(2)").addClass("hover");
		});
		$(".nav_top li:nth-child(3)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(3)").addClass("active");
		});
		$(".hd_btm li:nth-child(3)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(3)").addClass("hover");
		});
		$(".nav_top li:nth-child(4)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(4)").addClass("active");
		});
		$(".hd_btm li:nth-child(4)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(4)").addClass("hover");
		});
		$(".nav_top li:nth-child(5)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(5)").addClass("active");
		});
		$(".hd_btm li:nth-child(5)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(5)").addClass("hover");
		});
		$(".nav_top li:nth-child(6)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(6)").addClass("active");
		});
		$(".hd_btm li:nth-child(6)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(6)").addClass("hover");
		});
		$(".nav_top li:nth-child(7)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(7)").addClass("active");
		});
		$(".hd_btm li:nth-child(7)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(7)").addClass("hover");
		});
		$(".nav_top li:nth-child(8)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(8)").addClass("active");
		});
		$(".hd_btm li:nth-child(8)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(8)").addClass("hover");
		});
		$(".nav_top li:nth-child(9)").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(".hd_btm li:nth-child(9)").addClass("active");
		});
		$(".hd_btm li:nth-child(9)").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(".nav_top li:nth-child(9)").addClass("hover");
		});

		$(".nav_top li").on("mouseenter", function() {
			$(".nav_top li").removeClass("hover");
			$(this).addClass("hover");
		});
		$(".nav_top li").on("mouseleave", function() {
			$(this).removeClass("hover");
		});
		$(".hd_btm li").on("mouseenter", function() {
			$(".hd_btm li").removeClass("active");
			$(this).addClass("active");
		});
		$(".hd_btm li").on("mouseleave", function() {
			$(this).removeClass("active");
		});
    });
    
    $("p.title").on('click', function () {
        $(".mobile_submenu").slideUp(500);
        $("p.title").removeClass('act');


        $(this).addClass('act')
        $(this).next(".mobile_submenu").slideToggle(500);
    });
    
    $(window).scroll(function() {
        let scrollTop = $(this).scrollTop();
        let lastScrollTop = 0;
        
        if(scrollTop >= 80) { // 숫자에 따라 아래로 스크롤 했을 때 사라지는 영역의 크기가 바뀝니다.
			if ((scrollTop > lastScrollTop) && (lastScrollTop>0)) { /* &&: AND, 두 값이 모두 참이어야 값이 출력 */
				/* 화면에 나오지 않을 때: top값을 마이너스로 요소가 보이지 않게 사용해야함 */
				$("header").css("top","-80px");
				$('header').removeClass('scroll');
			  }else {
				$("header").css("top","0px");
				$('header').removeClass('scroll').addClass('scroll');
				}
	
			lastScrollTop = scrollTop;
		} else {
			$('header').removeClass('scroll');
		
		}
    });
</script>
