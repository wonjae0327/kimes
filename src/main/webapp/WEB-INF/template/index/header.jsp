<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-01
  Time: 오후 2:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="menu_bg"></div>
<div class="sidebar_menu">
    <div class="close_btn"><a href="#"><i class="xi-close-thin"></i></a></div>

    <div class="accordion-box">
        <ul class="list">
            <li>
                <p class="title"> 참관안내</p>
                <div class="mobile_submenu">
                    <a href="/kor/about/info.asp">전시회안내</a>
                    <a href="/kor/about/traffic_coex.asp">교통안내</a>
                    <a href="/kor/about/hotel.asp">호텔안내</a>
                    <a href="https://www.coex.co.kr/guide/amenities/" target="_blank">편의시설</a>
                </div>
            </li>
            <li>
                <p class="title"> 참관사전등록</p>
                <div class="mobile_submenu">
                    <a href="<c:url value='/enroll/regist' />">참관사전등록</a>
                    <a href="/kor/enroll/regist_search.asp">사전등록 조회</a>
                </div>
            </li>
            <li>
                <p class="title"> 참가업체검색</p>
                <div class="mobile_submenu">
                    <a href="/kor/search/company_list.asp">참가업체별 검색</a>
                    <a href="/kor/search/brand_list.asp">제조사별 검색</a>
                    <a href="/kor/search/product.asp">전시품별 검색</a>
                    <a href="/kor/search/floor.asp">전시장도면 검색</a>
                </div>
            </li>
            <li>
                <p class="title"> 뉴스</p>
                <div class="mobile_submenu">
                    <a href="/kor/board/news.asp">참가업체 뉴스</a>
                    <a href="/kor/board/news_industry.asp">업계 뉴스</a>
                </div>
            </li>
            <!--li>
                <p class="title"> 세미나</p>
                <div class="mobile_submenu">
                    <a href="/kor/seminar/schedule.asp">세미나 일정</a>
                    <a href="/kor/seminar/info01.asp">세미나실 안내</a>
                    <a href="/kor/seminar/seminar_search.asp">세미나 신청 조회</a>
                </div>
            </li-->
            <li>
                <p class="title"> 세미나</p>
                <div class="mobile_submenu">
                    <a href="/kor/seminar/seminar.asp"><span>의료산업 및<br> 의학술 세미나</span></a>
                    <!--a href="/kor/seminar/seminar06.asp"><span>KOTRA 글로벌 의료기기 컨퍼런스</span></a-->
                    <a href="/kor/seminar/seminar.asp?category=6&cate_num=3"><span>의료인 전문 컨퍼런스</span></a>
                    <a href="/kor/seminar/seminar_search.asp">세미나 신청 조회</a>
                </div>
            </li>
            <li>
                <p class="title"> 자료실</p>
                <div class="mobile_submenu">
                    <a href="/kor/board/notice_list.asp">공지사항</a>
                    <a href="/kor/board/notice_list.asp?site_type=E">지원사업 안내</a>
                    <a href="/kor/board/pr_list.asp">보도자료</a>
                    <a href="/kor/board/gallery.asp">사진자료</a>
                    <a href="/kor/board/kimes_story.asp">영상자료</a>
                    <a href="/kor/search/company_old.asp">지난 전시회 자료</a>
                </div>
            </li>
            <li>
                <p class="title"> 참가안내</p>
                <div class="mobile_submenu">
                    <a href="/kor/visit/exhibit.asp">참가신청</a>
                    <a href="/kor/exhibitor/login.asp" target="_blank">참가업체 전용 로그인</a>
                    <a href="/kor/visit/download.asp">기타 양식 다운로드</a>
                    <a href="https://ndata.sysforu.co.kr/login/login.asp" target="_blank">참관객 데이터</a>
                </div>
            </li>
            <li>
                <p class="title"> 수출상담회</p>
                <div class="mobile_submenu">
                    <a href="/kor/gmep/gmep.asp">GMEP 수출상담회</a>
                </div>
            </li>
            <li>
                <p class="title"> 동시개최행사</p>
                <div class="mobile_submenu">
                    <a href="/kor/medicomtek/index.asp">MEDICAL KOREA</a>
                    <a href="/kor/medicomtek/index2.asp">MEDICOMTEK</a>
                </div>
            </li>
        </ul>
    </div>
</div>

<header>
    <div class="nav_top">
        <div class="logo">
            <p><a href="<c:url value='/' />"><img src="<c:url value='/main_images/common/logo_kimes.png' />" alt="KIMES 2022 LOGO"/></a></p>
        </div>

        <ul class="gnb hd_inner">
            <li><a href="/kor/about/info.asp">참관안내</a></li>
            <li><a href="<c:url value='/enroll/regist' />">참관사전등록</a></li>
            <li><a href="/kor/search/company_list.asp">참가업체검색</a></li>
            <li><a href="/kor/board/news.asp">뉴스</a></li>
            <li><a href="/kor/seminar/seminar.asp">세미나</a></li>
            <li><a href="/kor/board/notice_list.asp?site_type=G">자료실</a></li>
            <li><a href="/kor/visit/exhibit.asp">참가안내</a></li>
            <li><a href="/kor/gmep/gmep.asp">수출상담회</a></li>
            <li><a href="/kor/medicomtek/index.asp">동시개최행사</a></li>
        </ul>

        <div class="btn_language">
            <p class="btn_mo_menu"><a class="menu_btn"><i class="xi-hamburger-back"></i> <span>MENU</span></a></p>
            <p><a href="https://blog.naver.com/kimes1980" target="_blank"><i class="xi-blogger"></i></a></p>
            <p><a href="https://www.instagram.com/kimes_exhibition/" target="_blank"><i class="xi-instagram"></i></a></p>
            <p><a href="https://www.facebook.com/KIMESexhibition" target="_blank"><i class="xi-facebook"></i> </a></p>
            <p><a href="/eng/"><!--i class="xi-globus"></i--> <span style="display:block;">ENG</span></a></p>
        </div>

    </div> <!-- end class nav_top -->
    <div class="hd_btm">
    	<ul class="hd_inner flex_height">
    		<li>
				<a href="/kor/about/info.asp">전시회안내</a>
				<a href="/kor/about/traffic_coex.asp">교통안내</a>
				<a href="/kor/about/hotel.asp">호텔안내</a>
				<a href="https://www.coex.co.kr/guide/amenities/" target="_blank">편의시설</a>
			</li>
			<li>
				<a href="<c:url value='/enroll/regist' />">참관사전등록</a>
				<a href="/kor/enroll/regist_search.asp">사전등록조회</a>
			</li>
			<li>
				<a href="/kor/search/company_list.asp">참가업체별 검색</a>
				<a href="/kor/search/brand_list.asp">제조사별 검색</a>
				<a href="/kor/search/product.asp">전시품별 검색</a>
				<a href="/kor/search/floor.asp">전시장도면 검색</a>
			</li>
			<li>
				<a href="/kor/board/news.asp">참가업체 뉴스</a>
				<a href="/kor/board/news_industry.asp">업계 뉴스</a>
			</li>
			<!--li>
				<a href="/kor/seminar/schedule.asp">세미나 일정</a>
				<a href="/kor/seminar/info01.asp">세미나실 안내</a>
				<a href="/kor/seminar/seminar_search.asp">세미나 신청 조회</a>
			</li-->
			<li>
				<a href="/kor/seminar/seminar.asp" style="line-height:25px;"><span>의료산업 및<br> 의학술 세미나</span></a>
				<!--a href="/kor/seminar/seminar06.asp" style="line-height:25px;"><span>KOTRA 글로벌 의료기기 컨퍼런스</span></a-->
				<a href="/kor/seminar/seminar.asp?category=6&cate_num=3"><span>의료인 전문 컨퍼런스</span></a>
				<a href="/kor/seminar/seminar_search.asp">세미나 신청 조회</a>
			</li>
			
			<li>
				<a href="/kor/board/notice_list.asp?site_type=G">공지사항</a>
				<a href="/kor/board/notice_list.asp?site_type=E">지원사업 안내</a>
				<a href="/kor/board/pr_list.asp">보도자료</a>
				<a href="/kor/board/gallery.asp">사진자료</a>
				<a href="/kor/board/kimes_story.asp">영상자료</a>
				<a href="/kor/search/company_old.asp">지난 전시회 자료</a>
				<a href="/kor/visit/download.asp">기타 양식 다운로드</a>
			</li>
			<li>
				<a href="/kor/visit/exhibit.asp">참가신청</a>
				<a href="/kor/exhibitor/login.asp">참가업체 전용 로그인</a>
				<a href="https://ndata.sysforu.co.kr/login/login.asp" target="_blank" title="새창열림" >참관객 데이터</a>
			</li>
			<li>
				<a href="/kor/gmep/gmep.asp">GMEP 수출상담회</a>
			</li>
			<li>
				<a href="/kor/medicomtek/index.asp">MEDICAL KOREA</a>
				<a href="/kor/medicomtek/index2.asp">MEDICOMTEK</a>
			</li>
		</ul>
	</div>
</header>