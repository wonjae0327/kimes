<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-04
  Time: 오후 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="footer">
    <div class="footer_cont">
        <div class="footer_btn">
            <a href="#wrap" class="btn_ico top"><span>TOP</span></a>
        </div>
        <div class="footer_addr">
            <address>
                <span class="addr">서울특별시 강남구 영동대로 511 트레이드타워 2001호</span>
                <span>TEL : 82(2) 551-0102<span class="bar">|</span>FAX : 82(2) 551-0103<span class="bar">|</span></span>
                <span>E-MAIL : <a href="mailto:kimes@kimes.kr">KIMES@KIMES.KR</a><span class="bar">|</span></span>
                <span>개인정보관리책임자 : 김종완</span>
            </address>
            <p>COPYRIGHT &copy; KIMES ALL RIGHTS RESERVED.</p>
        </div>
    </div>
</div>