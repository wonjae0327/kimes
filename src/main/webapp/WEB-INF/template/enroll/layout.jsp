<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-04
  Time: 오후 1:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta property="og:image" content="/asset/images/common/meta_img_2024_2.jpg">
    <meta name="facebook-domain-verification" content="ej36ke09cip3ojei0qpj2qkjwe1qwq"/>
    <title>KIMES 2024 제39회 국제의료기기병원설비 전시회</title>

    <!-- stylesheet -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/asset/css/style.css' />">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css">
    <!-- stylesheet -->

    <!-- javascript -->
    <script type="text/javascript" src="<c:url value="/asset/js/jquery-1.11.3.min.js" />"></script>
    <script type="text/javascript" src="<c:url value='/asset/js/jquery.easing.js' />"></script>
    <script type="text/javascript" src="<c:url value='/asset/js/jquery.bxslider.js' />"></script>
    <script type="text/javascript" src="<c:url value='/asset/js/civerView.js' />"></script>
    <script type="text/javascript" src="<c:url value='/asset/js/default.js' />"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-226008458-1"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.7.16"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-226008458-1');
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV5RZ87"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '272971297758190');
        fbq('track', 'PageView');
        fbq('track', 'ViewContent');
    </script>

    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=272971297758190&ev=PageView
        &noscript=1"/>
    </noscript>

    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-766869004"></script>

    <script>

        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());


        gtag('config', 'AW-766869004');

    </script>

    <script>
        fbq('track', 'CompleteRegistration')
    </script>
    <!-- Meta Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '272971297758190');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=272971297758190&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->


    <!-- Global site tag (gtag.js) - Google Ads: 10858045346 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-10858045346"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());


        gtag('config', 'AW-10858045346');
    </script>


    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-11111273328"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());


        gtag('config', 'AW-11111273328');
    </script>


    <!-- Google tag (gtag.js) 240226 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-16469441167"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());


        gtag('config', 'AW-16469441167');
    </script>

    <script type="text/javascript">
        // <![CDATA[
        jQuery(function ($) {
            gnb([2, 1, 1]);
        });
        // ]]>
    </script>

    <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>

    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=272971297758190&ev=PageView&noscript=1"/>
    </noscript>
    <!-- javascript -->

    <style>
        .bx_type05 {
            display: block;
            padding: 40px;
            border: 1px solid #ddd;
        }

        .bx_type05:after {
            display: block;
            clear: both;
            content: ''
        }

        .bx_type05 li {
            float: left;
        }

        .bx_type05 li:first-of-type {
            width: 23%;
        }

        .bx_type05 li:last-of-type {
            width: 77%;
        }

        .bx_type05 li p {
            font-size: 19px;
            margin-top: 60px;
            line-height: normal;
        }

        @media screen and (max-width: 800px) {
            .bx_type05 li {
                float: none;
                display: block;
            }

            .bx_type05 li:first-of-type {
                width: 100%;
                margin-bottom: 15px;
            }

            .bx_type05 li:last-of-type {
                width: 100%;
            }

            .bx_type05 li p {
                font-size: 15px;
                margin-top: 50px;
                text-align: center;
            }

            .bx_type05 li > img {
                width: 100%;
            }
        }

        .ad_banner {
            overflow: hidden;
        }

        .ad_banner > ul {
            width: 100%;
        }

        .ad_banner > ul > li:nth-child(1) {
            float: left;
            text-align: center;
            width: 100%;
        }

        .ad_banner > ul > li:nth-child(2) {
            float: left;
            width: 50%;
        }

        @media screen and (max-width: 1200px) {
            .ad_banner > ul > li {
                float: none;
                width: 100%;
                margin-bottom: 10px;
            }

            .ad_banner > ul > li img {
                width: 100%;
            }
        }
    </style>

</head>
<body>
<div id="wrap">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="body"/>
    <tiles:insertAttribute name="footer"/>
</div>

<script type="text/javascript">
    $(function () {
        $("#NC").click(function () {
            if ($("#NC").is(":checked")) {
                $("#company_name_kor").attr("disabled", true).attr("readonly", true);
                $("#company_name_kor").css("background-color", "silver");

                $("#part_kor").attr("disabled", true).attr("readonly", true);
                $("#part_kor").css("background-color", "silver");

                $("#position_kor").attr("disabled", true).attr("readonly", true);
                $("#position_kor").css("background-color", "silver");
            } else {
                $("#company_name_kor").attr("disabled", false).attr("readonly", false);
                $("#company_name_kor").css("background-color", "white");

                $("#part_kor").attr("disabled", false).attr("readonly", false);
                $("#part_kor").css("background-color", "white");

                $("#position_kor").attr("disabled", false).attr("readonly", false);
                $("#position_kor").css("background-color", "white");
            }
        });
        $("#mobile").keyup(function () {
            $(this).val($(this).val().replace(/[^0-9]/g, ""));
        });
    });

    var pre_regist = function () {
        var user_name_kor = $('#user_name_kor').val();
        var company_name_kor = $('#company_name_kor').val();
        var zipcode = $('#zipcode').val();
        var addr1 = $('#addr1').val();
        var addr2 = $('#addr2').val();
        var mobile = $('#mobile').val();
        var email = $('#email').val();
        var chkregist = $('#chk_regist').val();
        var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

        if (user_name_kor == '') {
            alert("성명을 입력해주세요.");
            $("input#user_name_kor").focus();
            return;
        }
        if (!($("input:radio[name='birth']").is(':checked'))) {
            alert("연령을 선택해주세요.");
            $("input:radio[name='birth']").focus();
            return;
        }
        if (!($("input:radio[name='sex']").is(':checked'))) {
            alert("성별을 선택해주세요.");
            $("input:radio[name='sex']").focus();
            return;
        }
        if (!($("input:radio[name='VISIT_TYPE']").is(':checked'))) {
            alert("구분을 선택해주세요.");
            $("input:radio[name='VISIT_TYPE']").focus();
            return;
        }
        if (chkregist == '') {
            alert("출품사 상담예약을 선택해주세요.");
            $("input#chk_regist").focus();
            return;
        }
        if ($("#NC").is(":checked")) {
        } else {
            if (company_name_kor == '') {
                alert("회사명을 입력해주세요.");
                $("input#company_name_kor").focus();
                return;
            }
        }
        if (zipcode == '') {
            alert("우편번호를 입력해주세요.");
            $("input#zipcode").focus();
            return;
        }
        if (addr1 == '') {
            alert("주소를 입력해주세요.");
            $("input#addr1").focus();
            return;
        }
        if (addr2 == '') {
            alert("상세주소를 입력해주세요.");
            $("input#addr2").focus();
            return;
        }
        if (mobile == '') {
            alert("휴대전화를 입력해주세요.");
            $("input#mobile").focus();
            return;
        }
        if (!($("input:radio[name='mbzine']").is(':checked'))) {
            alert("휴대전화 수신동의/거부를 선택해주세요.");
            $("input:radio[name='mbzine']").focus();
            return;
        }
        if (email == '') {
            alert("이메일을 입력해주세요.");
            $("input#email").focus();
            return;
        }

        if (!($("input:radio[name='webzine']").is(':checked'))) {
            alert("이메일 수신동의/거부를 선택해주세요.");
            $("input:radio[name='webzine']").focus();
            return;
        }
        if (!($("input:radio[name='opt_01']").is(':checked'))) {
            alert("직업을 선택해주세요.");
            $("input:radio[name='webzine']").focus();
            return;
        }
        if (!($("input:checkbox[name='opt_03']").is(':checked'))) {
            alert("관심분야는 하나 이상 체크해 주세요..");
            $("input:checkbox[name='opt_03']").focus();
            return;
        }
        if (!($("input:checkbox[name='opt_02']").is(':checked'))) {
            alert("참관목적은 하나 이상 체크해 주세요.");
            $("input:checkbox[name='opt_02']").focus();
            return;
        }
        if (!($("input:checkbox[name='opt_04']").is(':checked'))) {
            alert("참관경로는 하나 이상 체크해 주세요.");
            $("input:checkbox[name='opt_04']").focus();
            return;
        }
        if (!($("input:checkbox[name='opt_07']").is(':checked'))) {
            alert("참관예정일은 하나 이상 체크해 주세요.");
            $("input:checkbox[name='opt_07']").focus();
            return;
        }
        if (!($("input:checkbox[name='chk_agree']").is(':checked'))) {
            alert("개인정보 취급방침 및 활용에 동의해 주셔야 합니다.");
            $("input:checkbox[name='chk_agree']").focus();
            return;
        }
        $('#btn_area').hide();
        $('#frm').submit();

    };

    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '272971297758190');

    // 우편번호 찾기 화면을 넣을 element
    var element_layer = document.getElementById('layer');

    function closeDaumPostcode() {
        // iframe을 넣은 element를 안보이게 한다.
        element_layer.style.display = 'none';
    }

    function sample6_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function (data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    addr = data.roadAddress;
                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    addr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
                if (data.userSelectedType === 'R') {
                    // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                    // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                    if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                        extraAddr += data.bname;
                    }
                    // 건물명이 있고, 공동주택일 경우 추가한다.
                    if (data.buildingName !== '' && data.apartment === 'Y') {
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                    if (extraAddr !== '') {
                        extraAddr = ' (' + extraAddr + ')';
                    }
                    // 조합된 참고항목을 해당 필드에 넣는다.


                } else {

                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('zipcode').value = data.zonecode;
                document.getElementById("addr1").value = addr;
                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("addr2").focus();
            }
        }).open();
    }

    // 브라우저의 크기 변경에 따라 레이어를 가운데로 이동시키고자 하실때에는
    // resize이벤트나, orientationchange이벤트를 이용하여 값이 변경될때마다 아래 함수를 실행 시켜 주시거나,
    // 직접 element_layer의 top,left값을 수정해 주시면 됩니다.
    function initLayerPosition() {
        var width = 300; //우편번호서비스가 들어갈 element의 width
        var height = 460; //우편번호서비스가 들어갈 element의 height
        var borderWidth = 5; //샘플에서 사용하는 border의 두께

        // 위에서 선언한 값들을 실제 element에 넣는다.
        element_layer.style.width = width + 'px';
        element_layer.style.height = height + 'px';
        element_layer.style.border = borderWidth + 'px solid';
        // 실행되는 순간의 화면 너비와 높이 값을 가져와서 중앙에 뜰 수 있도록 위치를 계산한다.
        element_layer.style.left = (((window.innerWidth || document.documentElement.clientWidth) - width) / 2 - borderWidth) + 'px';
        element_layer.style.top = (((window.innerHeight || document.documentElement.clientHeight) - height) / 2 - borderWidth) + 'px';
    }

    function noment() {
        document.getElementById("meeting").innerHTML = "";
    }

    new Vue({
        el: "#container",
        data: {
            interest: [
                {value: "A", text: "진찰용 및 진단용기기"},
                {value: "B", text: "임상, 검사용기기"},
                {value: "C", text: "방사선 관련기기"},
                {value: "D", text: "수술 관련기기 및 장비"},
                {value: "E", text: "치료 관련기기"},
                {value: "F", text: "재활의학, 물리치료기"},
                {value: "G", text: "안과 관련기기"},
                {value: "H", text: "치과 관련기기"},
                {value: "I", text: "중앙공급실 관련기기"},
                {value: "J", text: "병원설비 및 응급장비"},
                {value: "K", text: "의료정보시스템"},
                {value: "L", text: "한방 관련기기"},
                {value: "M", text: "제약 관련기기"},
                {value: "N", text: "피부미용 및 건강 관련기기"},
                {value: "O", text: "의료기기 부품, 소재/서비스"},
                {value: "P", text: "의료용품, 소모품, 기타"},
                {value: "Q", text: "방역 관련기기"},
                {value: "R", text: "동물의료기기"},
            ],
            purpose: [
                {value: "A", text: "기술, 정보수집"},
                {value: "B", text: "구매상담"},
                {value: "C", text: "수출입알선"},
                {value: "D", text: "시장조사"},
                {value: "E", text: "일반관람"},
            ]
        }
    });
</script>
</body>
</html>
