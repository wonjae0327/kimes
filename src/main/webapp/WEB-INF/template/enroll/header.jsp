<%--
  Created by IntelliJ IDEA.
  User: USER
  Date: 2024-04-04
  Time: 오후 1:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="header"><!-- gnb 오픈 : gnb_open 추가 -->
    <div class="header_cont">
        <div class="header_side">
            <h1><a href="<c:url value='/' />">KIMES</a></h1>

            <!-- util -->
            <div class="util">
                <a href="https://blog.naver.com/kimes1980" target="_blank" class="blog"><span>Blog</span></a>
                <a href="https://www.instagram.com/kimes_exhibition/" target="_blank" class="instagram"><span>Instagram</span></a>
                <a href="https://www.facebook.com/KIMESexhibition" target="_blank" class="facebook"><span>Facebook</span></a>
                <a href="#" class="contact"><span>Contact Us</span></a>
                <a href="#" class="eng"><span>ENG</span></a>
                <a href="#" class="menu"><span>메뉴보기</span></a><!-- gnb 오픈 -->
            </div>
            <!-- util -->

            <!-- gnb_wrap -->
            <div class="gnb_wrap">
                <div class="gnb_header">
                    <a href="/kor/enroll/regist.asp"><span>사전등록</span></a>
                    <a href="/kor/visit/exhibit.asp"><span>참가신청</span></a>
                </div>
                <!-- gnb -->
                <div class="gnb">
                    <ul>
                        <li><!-- gnb 오픈 : on 클래스 추가 -->
                            <a href="/kor/about/info.asp"><span>참관안내</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/about/info.asp"><span>전시회안내</span></a></li>
                                <!--li><a href="/kor/about/floor.asp"><span>전시장도면</span></a></li-->
                                <li><a href="/kor/about/traffic_coex.asp"><span>교통안내</span></a></li>
                                <li><a href="/kor/about/hotel.asp"><span>호텔안내</span></a></li>
                                <li><a href="https://www.coex.co.kr/guide/amenities/" target="_blank">편의시설</a></li>
                                <!-- <li><a href="/kor/about/tour.asp"><span>투어프로그램</span></a></li> -->
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/enroll/regist.asp"><span>참관사전등록</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/enroll/regist.asp"><span>참관사전등록</span></a></li>
                                <li><a href="/kor/enroll/regist_search.asp"><span>사전등록 조회</span></a></li>
                                <!--li><a href="/kor/enroll/consult.asp"><span>참가사 상담예약</span></a></li>
                                <li><a href="/kor/enroll/consult_search.asp"><span>상담예약 조회</span></a></li-->
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/search/company_list.asp"><span>참가업체검색</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/search/company_list.asp"><span>참가업체별 검색</span></a></li>
                                <li><a href="/kor/search/brand_list.asp"><span>제조사별 검색</span></a></li>
                                <li><a href="/kor/search/product.asp"><span>전시품별 검색</span></a></li>
                                <li><a href="/kor/search/floor.asp"><span>전시장도면 검색</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/board/news.asp"><span>뉴스</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/board/news.asp"><span>참가업체 뉴스</span></a></li>
                                <li><a href="/kor/board/news_industry.asp"><span>업계 뉴스</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/seminar/seminar.asp"><span>세미나</span></a>
                            <!--/kor/seminar/schedule.asp-->
                            <ul class="dep01">
                            <li><a href="/kor/seminar/seminar.asp"><span>의료산업 및<br> 의학술 세미나</span></a></li>
                            <!--li><a href="/kor/seminar/seminar.asp"><span>KOTRA 글로벌 의료기기 컨퍼런스</span></a></li-->
                            <!--/kor/seminar/seminar06.asp-->
                            <li><a href="/kor/seminar/seminar.asp?category=6&cate_num=3"><span>의료인 전문 컨퍼런스</span></a></li>
                            <!--/kor/seminar/seminar05_01.asp--->
                            <!--li><a href="/kor/seminar/schedule.asp"><span>세미나 일정</span></a></li>
                            <li><a href="/kor/seminar/info01.asp"><span>세미나실 안내</span></a></li-->
                            <li><a href="/kor/seminar/seminar_search.asp"><span>세미나 신청 조회</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/board/notice_list.asp?site_type=G"><span>자료실</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/board/notice_list.asp?site_type=G"><span>공지사항</span></a></li>
                                <li><a href="/kor/board/notice_list.asp?site_type=E"><span>지원사업 안내</span></a></li>
                                <!-- <li><a href="/kor/board/report_list.asp"><span>결과자료</span></a></li> -->
                                <li><a href="/kor/board/pr_list.asp"><span>보도자료</span></a></li>
                                <li><a href="/kor/board/gallery.asp"><span>사진자료</span></a></li>
                                <li><a href="/kor/board/kimes_story.asp"><span>영상자료</span></a></li>
                                <li class="add">
                                    <a href="/kor/search/company_old.asp"><span>지난 전시회 자료</span></a>
                                    <ul class="dep02">
                                        <li><a href="/kor/search/company_old.asp"><span>참가업체별 검색</span></a></li>
                                        <li><a href="/kor/search/brand_old.asp"><span>제조사별 검색</span></a></li>
                                        <li><a href="/kor/search/product_old.asp"><span>전시품별 검색</span></a></li>
                                        <li><a href="/kor/search/floor_old.asp"><span>전시장도면 검색</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="/kor/visit/download.asp"><span>기타 양식 다운로드</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/visit/exhibit.asp"><span>참가안내</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/visit/exhibit.asp"><span>참가신청</span></a></li>
                                <li><a href="/kor/exhibitor/login.asp" target="_blank"><span>참가업체 전용 로그인</span></a></li>
                                <!--<li><a href="/kor/visit/download.asp"><span>기타양식 다운로드</span></a></li>-->
                                <li><a href="https://ndata.sysforu.co.kr/login/login.asp" target="_blank" title="새창열림"><span>참관객 데이터</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/gmep/gmep.asp"><span>수출상담회</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/gmep/gmep.asp"><span>GMEP 수출상담회</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/kor/medicomtek/index.asp"><span>동시개최행사</span></a>
                            <ul class="dep01">
                                <li><a href="/kor/medicomtek/index.asp"><span>MEDICAL KOREA</span></a></li>
                                <li><a href="/kor/medicomtek/index2.asp"><span>MEDICOMTEK</span></a></li>
                            </ul>
                        </li>
                        <!--li class="hide_pc">
                            <a href="#">가이드</a>
                            <ul class="dep01">
                                <li><a href="/kor/guide/greeting.asp"><span>인사말</span></a></li>
                                <li><a href="/kor/guide/contact.asp"><span>사무국 연락처</span></a></li>
                                <!--li><a href="/kor/guide/counseling.asp"><span>상담문의</span></a></li>
                                <li><a href="/kor/guide/personal.asp"><span>개인정보정책</span></a></li>
                                <li><a href="/kor/guide/email.asp"><span>이메일정책</span></a></li>
                            </ul>
                        </li-->
                    </ul>
                </div>
                <!--// gnb -->
                <a href="#" class="btn_ico close_gnb"><span>닫기</span></a>
            </div>
            <!-- gnb_wrap -->
        </div>
    </div>
</div>